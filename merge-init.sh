#!/bin/sh


merge()
{
	echo $1
	git subtree add --prefix=$1 rem-$1/master
}

merge osmo-st-core git://git.osmocom.org/smalltalk/osmo-st-core
merge osmo-st-logging git://git.osmocom.org/smalltalk/osmo-st-logging
merge osmo-st-network git://git.osmocom.org/smalltalk/osmo-st-network
merge osmo-st-mgcp git://git.osmocom.org/smalltalk/osmo-st-mgcp
merge osmo-st-asn1 git://git.osmocom.org/smalltalk/osmo-st-asn1
merge osmo-st-gsm git://git.osmocom.org/smalltalk/osmo-st-gsm
merge osmo-st-msc git://git.osmocom.org/smalltalk/osmo-st-msc
merge osmo-st-sip git://git.osmocom.org/smalltalk/osmo-st-sip
merge osmo-st-testphone git://git.osmocom.org/smalltalk/osmo-st-testphone
merge petitparser git://gitorious.org/gnu-smalltalk-ports/petitparser
merge petitparser-tests git://gitorious.org/gnu-smalltalk-ports/petitparser-tests.git
merge soapopera git://gitorious.org/gnu-smalltalk-ports/soapopera.git
merge osmo-st-openbsc-test git://git.osmocom.org/smalltalk/osmo-st-openbsc-test
merge iliad-stable git://github.com/NicolasPetton/iliad-stable.git
merge grease git://github.com/NicolasPetton/Grease.git

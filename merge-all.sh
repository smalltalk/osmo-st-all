#!/bin/sh

merge()
{
	git subtree pull --prefix=$1 rem-$1 master
}

merge petitparser
merge petitparser-tests
merge soapopera
merge osmo-st-core
merge osmo-st-logging
merge osmo-st-network
merge osmo-st-mgcp
merge osmo-st-asn1
merge osmo-st-gsm
merge osmo-st-msc
merge osmo-st-sip
merge osmo-st-testphone
merge osmo-st-openbsc-test
merge iliad-stable
merge grease


Object subclass: SoapSampleCustomer [
    | name age dateOfBirth |
    
    <category: 'SOAP-Example-CustomComplexType'>
    <comment: nil>

    SoapSampleCustomer class >> customerA [
	<category: 'sample instances'>
	| cust |
	cust := self new.
	cust name: 'Masashi'.
	cust age: 36.
	cust dateOfBirth: (DateTime fromString: '1970-11-22T00:00:00-07:00').
	^cust
    ]

    SoapSampleCustomer class >> customerB [
	<category: 'sample instances'>
	| cust |
	cust := self new.
	cust name: 'Mike'.
	cust age: 26.
	cust dateOfBirth: (DateTime fromString: '1980-10-20T00:00:00-07:00').
	^cust
    ]

    SoapSampleCustomer class >> initialize [
	"SoapSampleCustomer initialize"

	<category: 'class initialization'>
	SoapEncoder complexTypeDict at: self name put: 'types:Customer'.
	SoapLiteralEncoder complexTypeDict at: self name put: 'types:Customer'.
	SoapDecoder complexTypeDict at: 'types:Customer' put: self
    ]

    age [
	"Answer the value of age"

	<category: 'accessing'>
	^age
    ]

    age: anObject [
	"Set the value of age"

	<category: 'accessing'>
	age := anObject
    ]

    dateOfBirth [
	"Answer the value of dateOfBirth"

	<category: 'accessing'>
	^dateOfBirth
    ]

    dateOfBirth: anObject [
	"Set the value of dateOfBirth"

	<category: 'accessing'>
	dateOfBirth := anObject
    ]

    name [
	<category: 'accessing'>
	^name
    ]

    name: anObject [
	"Set the value of name"

	<category: 'accessing'>
	name := anObject
    ]
]



Object subclass: SoapSampleGroup [
    | name members |
    
    <category: 'SOAP-Example-CustomComplexType'>
    <comment: nil>

    SoapSampleGroup class >> groupA [
	"SoapSampleGroup groupA"

	<category: 'sample instances'>
	| group |
	group := self new.
	group name: 'Squeak support'.
	group members add: SoapSampleCustomer customerA.
	group members add: SoapSampleCustomer customerB.
	^group
    ]

    SoapSampleGroup class >> initialize [
	"SoapSampleGroup initialize"

	<category: 'class initialization'>
	SoapEncoder complexTypeDict at: self name put: 'types:Groups'.
	SoapLiteralEncoder complexTypeDict at: self name put: 'types:Groups'.
	SoapDecoder complexTypeDict at: 'types:Groups' put: self
    ]

    members [
	"Answer the value of members"

	<category: 'accessing'>
	members ifNil: [members := OrderedCollection new].
	^members
    ]

    members: anObject [
	"Set the value of members"

	<category: 'accessing'>
	members := anObject
    ]

    name [
	<category: 'accessing'>
	^name
    ]

    name: anObject [
	"Set the value of name"

	<category: 'accessing'>
	name := anObject
    ]
]

TestCase subclass: SoapDecoderCustomTestCase [
    
    <comment: nil>
    <category: 'SOAP-TestCases'>

    dotNetSharedValuesEnvelopeXML2 [
	<category: 'fixtures'>
	^'<soap:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:types="http://tempuri.org/" xmlns:tns="http://tempuri.org/" 
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" 
xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
 <soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <types:FindCustomersResponse>
   <FindCustomersResult href="#id1"></FindCustomersResult>
  </types:FindCustomersResponse>
  <soapenc:Array id="id1" soapenc:arrayType="types:Customer[2]">
   <Item href="#id2"></Item>
   <Item href="#id3"></Item>
  </soapenc:Array>
  <types:Customer id="id2" xsi:type="types:Customer">
   <Name xsi:type="xsd:string">Ume</Name>
   <Age xsi:type="xsd:int">36</Age>
   <DateOfBirth 
xsi:type="xsd:dateTime">1970-11-22T00:00:00.0000000-07:00</DateOfBirth>
  </types:Customer>
  <types:Customer id="id3" xsi:type="types:Customer">
   <Name xsi:type="xsd:string">Ramon</Name>
   <Age xsi:type="xsd:int">31</Age>
   <DateOfBirth 
xsi:type="xsd:dateTime">1995-11-22T00:00:00.0000000-07:00</DateOfBirth>
  </types:Customer>
 </soap:Body>
</soap:Envelope>
'
    ]

    testDecodeDotNetSharedValuesXMLEnvelope2 [
	"self debug: #testDecodeDotNetSharedValuesXMLEnvelope2"

	<category: 'testing'>
	| originalUseDotNetStyleSharedValues envelopeBuilder returnEnvelope message resp value |
	SoapSampleCustomer initialize.
	
	[originalUseDotNetStyleSharedValues := SoapResponse 
		    useDotNetStyleSharedValues.
	SoapResponse useDotNetStyleSharedValues: true.
	envelopeBuilder := SoapEnvelopeBuilder new.
	returnEnvelope := envelopeBuilder 
		    buildSoapEnvelopeFromXmlString: self dotNetSharedValuesEnvelopeXML2.
	message := SoapMessage envelope: returnEnvelope.
	resp := SoapResponse fromSoapMessage: message.
	value := resp returnValue.
	self should: [value size = 2].
	self should: [value first name = 'Ume'].
	self should: [value first age = 36].
	self should: 
		[value first dateOfBirth 
		    = (DateTime fromString: '1970-11-22T00:00:00-07:00')].
	self should: [value second name = 'Ramon'].
	self should: [value second age = 31].
	self should: 
		[value second dateOfBirth 
		    = (DateTime fromString: '1995-11-22T00:00:00-07:00')]] 
		ensure: 
		    [SoapResponse 
			useDotNetStyleSharedValues: originalUseDotNetStyleSharedValues]
    ]

    testEncodeCustomStructType [
	"self debug: #testEncodeCustomStructType"

	<category: 'testing'>
	| enc cust elem elem2 |
	enc := SoapEncoder new.
	enc useSharedValues: false.
	cust := SoapSampleCustomer new.
	cust
	    name: 'John';
	    age: 25;
	    dateOfBirth: (DateTime fromString: '1980-11-22T00:00:00-07:00');
	    yourself.
	enc complexTypeDict at: #SoapSampleCustomer put: 'types:Customer'.
	elem := enc 
		    encodeSoapVariable: (SoapVariable name: 'customer' value: cust).
	self should: [elem name = 'customer'].
	"self should: [elem xsiType isNil]."
	self should: [elem xsiType = 'types:Customer'].
	self should: [(elem findChildNamed: 'name') value = 'John'].
	self should: [(elem findChildNamed: 'age') value = '25'].
	"BUG: FIXME:
	self should: 
		[(elem findChildNamed: 'dateOfBirth') value = '1980-11-22T00:00:00-07:00'].
	"
	elem2 := enc encodeSoapVariable: (SoapVariable 
			    name: 'customer'
			    value: cust
			    xsdType: (enc complexXsdTypeOf: cust)).
	self should: [elem2 name = 'customer'].
	self should: [elem2 xsiType = 'types:Customer'].
	self should: [(elem2 findChildNamed: 'name') value = 'John'].
	self should: [(elem2 findChildNamed: 'age') value = '25'].
	"BUG: FIXME:
	self should: 
		[(elem2 findChildNamed: 'dateOfBirth') value = '1980-11-22T00:00:00-07:00']
	"
    ]

    testEncodeCustomStructTypeHavingCustomStructs [
	"self debug: #testEncodeCustomStructTypeHavingCustomStructs"

	<category: 'testing'>
	| enc group elem members memA memB |
	enc := SoapEncoder new.
	group := SoapSampleGroup groupA.
	enc complexTypeDict at: #SoapSampleGroup put: 'types:Group'.
	enc complexTypeDict at: #SoapSampleCustomer put: 'types:Customer'.
	enc useSharedValues: true.
	elem := enc encodeSoapVariable: (SoapVariable name: 'group' value: group).
	self should: [elem name = 'group'].
	self should: [(elem findChildNamed: 'name') value = 'Squeak support'].
	members := elem findChildNamed: 'members'.
	self should: [members xsiType isNil].
	self should: [members arrayItemType = 'types:Customer'].
	self should: [members size = 2].
	memA := members children first.
	self should: [memA name = 'item'].
	self should: [memA xsiType = 'types:Customer'].
	self should: [(memA findChildNamed: 'name') value = 'Masashi'].
	self should: [(memA findChildNamed: 'age') value = '36'].
	"BUG: FIXME:
	self should: 
		[(memA findChildNamed: 'dateOfBirth') value = '1970-11-22T00:00:00-07:00'].
	"
	memB := members children second.
	self should: [memB name = 'item'].
	self should: [memB xsiType = 'types:Customer'].
	self should: [(memB findChildNamed: 'name') value = 'Mike'].
	self should: [(memB findChildNamed: 'age') value = '26'].
	"BUG: FIXME:
	self should: 
		[(memB findChildNamed: 'dateOfBirth') value = '1980-10-20T00:00:00-07:00']
	"
    ]

    testEncodeCustomStructTypeSharedInArray [
	"self debug: #testEncodeCustomStructTypeSharedInArray"

	<category: 'testing'>
	| enc cust cust2 elems elem first second third |
	enc := SoapEncoder new.
	cust := SoapSampleCustomer new.
	cust
	    name: 'John';
	    age: 25;
	    dateOfBirth: (DateTime fromString: '1980-11-22T00:00:00-07:00');
	    yourself.
	enc complexTypeDict at: #SoapSampleCustomer put: 'types:Customer'.
	cust2 := SoapSampleCustomer new.
	cust2
	    name: 'Taro';
	    age: 35;
	    dateOfBirth: (DateTime fromString: '1970-11-22T00:00:00-07:00');
	    yourself.
	enc useSharedValues: true.
	elems := (SoapEncArray encoder: enc) 
		    fromArray: (Array 
			    with: cust
			    with: cust2
			    with: cust)
		    named: 'resultElements'
		    elementType: 'ResultElement'.
	elem := enc 
		    encodeSoapVariable: (SoapVariable name: 'resultElements' value: elems).
	self should: [elem isArrayType].
	first := elem children first.
	self should: [first id notNil].
	self should: [(first findChildNamed: 'name') value = 'John'].
	self should: [(first findChildNamed: 'age') value = '25'].
	"BUG: FIXME:
	self should: 
		[(first findChildNamed: 'dateOfBirth') value = '1980-11-22T00:00:00-07:00'].
	"
	second := elem children second.
	self should: [second id isNil].
	self should: [(second findChildNamed: 'name') value = 'Taro'].
	self should: [(second findChildNamed: 'age') value = '35'].
	"BUG: FIXME:
	self should: 
		[(second findChildNamed: 'dateOfBirth') value = '1970-11-22T00:00:00-07:00'].
	"
	third := elem children third.
	self should: [third id isNil].
	self should: [third children isEmpty].
	self should: [first id = (third href copyFrom: 2) asNumber]
    ]

    testEncodeCustomStructTypeSharedInStruct [
	"self debug: #testEncodeCustomStructTypeSharedInStruct"

	<category: 'testing'>
	| enc cust cust2 elem struct1 a b c |
	enc := SoapEncoder new.
	cust := SoapSampleCustomer new.
	cust
	    name: 'John';
	    age: 25;
	    dateOfBirth: (DateTime fromString: '1980-11-22T00:00:00-07:00');
	    yourself.
	enc complexTypeDict at: #SoapSampleCustomer put: 'types:Customer'.
	cust2 := SoapSampleCustomer new.
	cust2
	    name: 'Taro';
	    age: 35;
	    dateOfBirth: (DateTime fromString: '1970-11-22T00:00:00-07:00');
	    yourself.
	struct1 := SoapEncStruct fromAssociations: 
			{#a -> cust.
			#b -> cust2.
			#c -> cust2}.
	enc useSharedValues: true.
	elem := enc 
		    encodeSoapVariable: (SoapVariable name: 'resultElements' value: struct1).
	self should: [elem name = 'resultElements'].
	a := elem findChildNamed: 'a'.
	self should: [a id isNil].
	self should: [(a findChildNamed: 'name') value = 'John'].
	self should: [(a findChildNamed: 'age') value = '25'].
	"FIXME: BUG:
	self should: 
		[(a findChildNamed: 'dateOfBirth') value = '1980-11-22T00:00:00-07:00'].
	"
	b := elem findChildNamed: 'b'.
	self should: [b id notNil].
	self should: [(b findChildNamed: 'name') value = 'Taro'].
	self should: [(b findChildNamed: 'age') value = '35'].
	"FIXME: BUG:
	self should: 
		[(b findChildNamed: 'dateOfBirth') value = '1970-11-22T00:00:00-07:00'].
	"
	c := elem findChildNamed: 'c'.
	self should: [c id isNil].
	self should: [c children isEmpty].
	self should: [b id = (c href copyFrom: 2) asNumber]
    ]
]


TestCase subclass: SoapDecoderCustomTestCase [
    
    <comment: nil>
    <category: 'SOAP-TestCases'>

    dotNetSharedValuesEnvelopeXML2 [
	<category: 'fixtures'>
	^'<soap:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:types="http://tempuri.org/" xmlns:tns="http://tempuri.org/" 
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" 
xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
 <soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <types:FindCustomersResponse>
   <FindCustomersResult href="#id1"></FindCustomersResult>
  </types:FindCustomersResponse>
  <soapenc:Array id="id1" soapenc:arrayType="types:Customer[2]">
   <Item href="#id2"></Item>
   <Item href="#id3"></Item>
  </soapenc:Array>
  <types:Customer id="id2" xsi:type="types:Customer">
   <Name xsi:type="xsd:string">Ume</Name>
   <Age xsi:type="xsd:int">36</Age>
   <DateOfBirth 
xsi:type="xsd:dateTime">1970-11-22T00:00:00.0000000-07:00</DateOfBirth>
  </types:Customer>
  <types:Customer id="id3" xsi:type="types:Customer">
   <Name xsi:type="xsd:string">Ramon</Name>
   <Age xsi:type="xsd:int">31</Age>
   <DateOfBirth 
xsi:type="xsd:dateTime">1995-11-22T00:00:00.0000000-07:00</DateOfBirth>
  </types:Customer>
 </soap:Body>
</soap:Envelope>
'
    ]

    testDecodeDotNetSharedValuesXMLEnvelope2 [
	"self debug: #testDecodeDotNetSharedValuesXMLEnvelope2"

	<category: 'testing'>
	| originalUseDotNetStyleSharedValues envelopeBuilder returnEnvelope message resp value |
	SoapSampleCustomer initialize.
	
	[originalUseDotNetStyleSharedValues := SoapResponse 
		    useDotNetStyleSharedValues.
	SoapResponse useDotNetStyleSharedValues: true.
	envelopeBuilder := SoapEnvelopeBuilder new.
	returnEnvelope := envelopeBuilder 
		    buildSoapEnvelopeFromXmlString: self dotNetSharedValuesEnvelopeXML2.
	message := SoapMessage envelope: returnEnvelope.
	resp := SoapResponse fromSoapMessage: message.
	value := resp returnValue.
	self should: [value size = 2].
	self should: [value first name = 'Ume'].
	self should: [value first age = 36].

	"BUG: broken time classes
	self should: 
		[value first dateOfBirth 
		    = (DateTime fromString: '1970-11-22T00:00:00-07:00')].
	"
	self should: [value second name = 'Ramon'].
	self should: [value second age = 31].
	"BUG: broken time classes in GST
	self should: 
		[value second dateOfBirth 
		    = (DateTime fromString: '1995-11-22T00:00:00-07:00')]
	"
	] 
		ensure: 
		    [SoapResponse 
			useDotNetStyleSharedValues: originalUseDotNetStyleSharedValues]
    ]
]

Eval [
    'From Pharo-1.1-11411 of 17 July 2010 [Latest update: #11411] on 19 February 2011 at 9:09:10 pm'
]


TestCase subclass: Base64MimeConverterTest [
    | message |
    
    <comment: 'This is the unit test for the class Base64MimeConverter. Unit tests are a good way to exercise the functionality of your system in a repeatable and automatic manner. They are therefore recommended if you plan to release anything. For more information, see: 	- http://www.c2.com/cgi/wiki?UnitTest	- http://minnow.cc.gatech.edu/squeak/1547	- the sunit class category'>
    <category: 'CollectionsTests-Streams'>

    setUp [
	<category: 'setup'>
	message := 'Hi There!' readStream
    ]

    testBase64Encoded [
	"self run: #testBase64Encoded"

	<category: 'tests'>
	| encoded |
	encoded := (Base64MimeConverter mimeEncode: message) contents.
	self assert: encoded = 'Hi There!' base64Encoded
    ]

    testMimeEncodeDecode [
	"self run: #testMimeEncodeDecode"

	<category: 'tests'>
	| encoded |
	encoded := Base64MimeConverter mimeEncode: message.
	self assert: encoded contents = 'SGkgVGhlcmUh'.
	self assert: (Base64MimeConverter mimeDecodeToBytes: encoded) contents asString
		    = message contents.

	"Encoding should proceed from the current stream position."
	message reset.
	message skip: 2.
	encoded := Base64MimeConverter mimeEncode: message.
	self assert: encoded contents = 'IFRoZXJlIQ=='.
    ]

    testOnByteArray [
	"self run: #testOnByteArray"

	<category: 'tests'>
	self 
	    assert: 'Hi There!' base64Encoded = 'Hi There!' asByteArray base64Encoded
    ]
]


TestCase subclass: SoXmlAttributesHolderTestCase [
    
    <comment: nil>
    <category: 'SOAP-TestCases'>

    testAsDictionary [
	<category: 'testing'>
	| attribsHolder dict |
	attribsHolder := SoXmlAttributesHolder new.
	attribsHolder putAttribute: #('key1' 'val1').
	attribsHolder putAttribute: #('key2' 'val2').
	dict := attribsHolder asDictionary.
	self should: [(dict at: 'key1') = 'val1'].
	self should: [(dict at: 'key2') = 'val2']
    ]

    testDeclareNamespaceURIs [
	<category: 'testing'>
	| attribsHolder |
	attribsHolder := SoXmlAttributesHolder new.
	attribsHolder declareNamespace: 'Foo1' uri: 'http://www.bar.org'.
	attribsHolder declareNamespace: 'Foo2' uri: 'http://www.baz.org'.
	self should: 
		[(attribsHolder declaredNamespaceURIFor: 'Foo1') = 'http://www.bar.org'].
	self should: 
		[(attribsHolder declaredNamespaceURIFor: 'Foo2') = 'http://www.baz.org']
    ]

    testDeclareNamespaces [
	<category: 'testing'>
	| attribsHolder |
	attribsHolder := SoXmlAttributesHolder new.
	attribsHolder declareNamespace: 'Foo1' uri: 'http://www.bar.org'.
	attribsHolder declareNamespace: 'Foo2' uri: 'http://www.baz.org'.
	self 
	    should: [attribsHolder declaredNamespacePrefixes = (Set with: 'Foo1' with: 'Foo2')]
    ]

    testDeclareNamespacesAndPutAttributes [
	<category: 'testing'>
	| attribsHolder ansStr |
	attribsHolder := SoXmlAttributesHolder new.
	attribsHolder declareNamespace: 'Foo1' uri: 'http://www.bar.org'.
	attribsHolder putAttribute: #('key1' 'val1').
	ansStr := ' xmlns:Foo1="http://www.bar.org" key1="val1"'.
	self should: [ansStr = attribsHolder printString]
    ]

    testPutAttributes [
	<category: 'testing'>
	| attribsHolder |
	attribsHolder := SoXmlAttributesHolder new.
	attribsHolder putAttribute: #('key1' 'val1').
	self should: ['val1' = (attribsHolder getAttributeNamed: 'key1')].
	attribsHolder putAttribute: #('key1' 'val2').
	self shouldnt: ['val1' = (attribsHolder getAttributeNamed: 'key1')].
	self should: ['val2' = (attribsHolder getAttributeNamed: 'key1')]
    ]
]



TestCase subclass: SoXmlUtilTestCase [
    
    <comment: nil>
    <category: 'SOAP-TestCases'>

    compString [
	<category: 'fixtures'>
	^self normalString , self convString
    ]

    convString [
	<category: 'fixtures'>
	^'<>''"&'
    ]

    nameapacedSimpleXmlString [
	<category: 'fixtures'>
	^'<m:Bar a="1" b="2" xmlns:m="http://soapinterop.org/">
		<bar>cont</bar>
	</m:Bar>'
    ]

    namespacedSimpleXmlElement [
	<category: 'fixtures'>
	^SoXmlUtil parseXml: self nameapacedSimpleXmlString
    ]

    normalString [
	<category: 'fixtures'>
	^'SoapOpera is a Squeak SOAP implementaion written by Masashi Umezawa'
    ]

    simpleXmlElement [
	<category: 'fixtures'>
	^SoXmlUtil parseXml: self simpleXmlString
    ]

    simpleXmlElementWithText [
	<category: 'fixtures'>
	^SoXmlUtil parseXml: self simpleXmlStringWithText
    ]

    simpleXmlString [
	<category: 'fixtures'>
	^'<Foo a="1" b="2">
		<bar>cont</bar>
	</Foo>'
    ]

    simpleXmlStringWithText [
	<category: 'fixtures'>
	^'<Foo a="1" b="2">
		some text...
		<bar>cont</bar>
	</Foo>'
    ]

    testAttribDict [
	<category: 'testing'>
	| elem dic1 dic2 |
	elem := self simpleXmlElement.
	dic1 := SoXmlUtil attribDictFrom: elem.
	dic2 := Dictionary new.
	dic2 at: 'a' put: '1'.
	dic2 at: 'b' put: '2'.
	self should: [dic1 = dic2]
    ]

    testComplexStringWrite [
	<category: 'testing'>
	| str1 str2 str3 |
	str1 := self compString.
	str2 := SoXmlUtil asXmlText: str1.
	str3 := self normalString , '&lt;&gt;&apos;&quot;&amp;'.
	self shouldnt: [str1 = str2].
	self should: [str2 = str3]
    ]

    testConvStringWrite [
	<category: 'testing'>
	| str1 str2 str3 |
	str1 := self convString.
	str2 := SoXmlUtil asXmlText: str1.
	str3 := '&lt;&gt;&apos;&quot;&amp;'.
	self shouldnt: [str1 = str2].
	self should: [str2 = str3]
    ]

    testElementName [
	<category: 'testing'>
	| elem nm |
	elem := self simpleXmlElement.
	nm := SoXmlUtil elementNameFrom: elem.
	self should: [nm = 'Foo']
    ]

    testElementShortName [
	<category: 'testing'>
	| elem2 nm2 |
	elem2 := self namespacedSimpleXmlElement.
	nm2 := SoXmlUtil elementShortNameFrom: elem2.
	self should: [nm2 = 'Bar']
    ]

    testElementsWithoutTexts [
	<category: 'testing'>
	| elem1 elems1 elem2 elems2 |
	elem1 := self simpleXmlElement.
	elems1 := SoXmlUtil elementsWithoutTextsFrom: elem1.
	elem2 := self simpleXmlElementWithText.
	elems2 := SoXmlUtil elementsWithoutTextsFrom: elem2.
	self should: [elems1 size = elems2 size].
	self should: [elems1 printString = elems2 printString]
    ]

    testNormalStringWrite [
	<category: 'testing'>
	| str1 str2 |
	str1 := self normalString.
	str2 := SoXmlUtil asXmlText: str1.
	self should: [str1 = str2]
    ]
]



TestCase subclass: SoXmlWrapElementTestCase [
    
    <comment: nil>
    <category: 'SOAP-TestCases'>

    testElemPrint1 [
	<category: 'testing'>
	| fixString wStr xmlElem |
	fixString := '<Foo>bar</Foo>
'.
	wStr := WriteStream on: (String new: 16).
	xmlElem := SoXmlWrapElement new.
	xmlElem name: 'Foo'.
	xmlElem value: 'bar'.
	xmlElem printXmlOn: wStr.
	self should: [wStr contents = fixString]
    ]

    testElemPrint2 [
	<category: 'testing'>
	| fixString wStr xmlElem child |
	fixString := '<SMIX>
 <interchangeUnit>
  <classDefinition className="Customer">
   <instVarNames>name address tel</instVarNames>
  </classDefinition>
 </interchangeUnit>
</SMIX>
'.
	wStr := WriteStream on: (String new: 16).
	xmlElem := SoXmlWrapElement new.
	xmlElem name: 'SMIX'.
	child := xmlElem createChildNamed: 'interchangeUnit'.
	child := child createChildNamed: 'classDefinition'.
	child putAttribute: #(#className #Customer).
	child := child createChildNamed: 'instVarNames'.
	child value: 'name address tel'.
	xmlElem printXmlOn: wStr.
	self should: [wStr contents = fixString]
    ]
]



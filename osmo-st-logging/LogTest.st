"
 (C) 2010 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: #OsmoLogging.

Osmo.LogArea subclass: LogAreaSCCP [
    LogAreaSCCP class >> areaName [
        ^ #sccp
    ]

    LogAreaSCCP class >> areaDescription [
        ^ 'SCCP related'
    ]

    LogAreaSCCP class >> default [
        ^ self new
            enabled: true;
            minLevel: LogLevel debug;
            yourself        
    ]
]

Eval [
    | syslog |
    '123' logDebug: 'TEST' area: #sccp.

    '123' logException: 'TEST' area: #sccp.


    "SYSLOG"
    syslog := (Osmo.LogTargetSyslog
                openlog: 'ow' option: 0 facility: Osmo.LogTargetSyslog LOG_USER).
    syslog prefix: 'abc:'.
    Object logManager target: syslog.


    '123' logError: 'Error message' area: #sccp.

    [
        '123' error: 'ABC DEF'.
    ] on: Error do: [:e |
        e logException: 'Exception... ' area: #sccp.
    ].

    Object logManager target:
            (Osmo.LogTargetSyslog
                    openlog: 'ow2' option: 0 facility: Osmo.LogTargetSyslog LOG_USER).
    '123' logError: 'Error message' area: #sccp.

    [
        '123' error: 'ABC DEF'.
    ] on: Error do: [:e |
        e logException: 'Exception... ' area: #sccp.
    ].
]

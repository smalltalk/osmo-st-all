"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

LogTarget subclass: LogTargetSyslog [
    | prefix |
    <comment: 'I can log everything to the syslog.'>
    <category: 'OsmoLogging-Syslog'>

    LogMap := nil.

    LogTargetSyslog class >> LOG_USER [
        <category: 'c-facility'>
        ^ 8
    ]

    LogTargetSyslog class >> logLevelMap [
        ^ LogMap ifNil: [ LogMap := Dictionary new
            at: LogLevel debug put: 7;
            at: LogLevel info  put: 6;
            at: LogLevel notice put: 5;
            at: LogLevel error put: 3;
            yourself
        ]
    ]

    LogTargetSyslog class >> openlog: aIdent option: aOption facility: aFacility [
        "Free any previous string"
        SYSLOG_NAME ifNotNil: [
            SYSLOG_NAME free.
            Smalltalk at: #SYSLOG_NAME put: nil.
        ].

        Smalltalk at: #SYSLOG_NAME put: aIdent asCData.

        self c_closelog.
        self c_openlog: SYSLOG_NAME opt: aOption facility: aFacility.
        ^ self new
    ]

    LogTargetSyslog class >> c_openlog: ident opt: aOpt facility: aFac [
        <category: 'c-interface'>
        <cCall: 'openlog' returning: #void args: #(#cObject #int #int)>
    ]

    LogTargetSyslog class >> c_syslog: prio fmt: aFormat args: args[
        <category: 'c-interface'>
        <cCall: 'syslog' returning: #void args: #(#int #string #variadic)>
    ]

    LogTargetSyslog class >> c_closelog [
        <category: 'c-interface'>
        <cCall: 'closelog' returning: #void args: #()>
    ]

    LogTargetSyslog class >> initialize [
        <category: 'c-interface'>
        DLD addLibrary: 'libc'.

        "Workaround Debian multiarch issues in finding libc"
        DLD addLibrary: 'libm'.

	ObjectMemory addDependent: self.
    ]

    LogTargetSyslog class >> update: aSymbol [
        "We need to forget the C String we have allocated as we are running
         in a new VM right now. Maybe we will be re-opened by someone."
        aSymbol = #returnFromSnapshot ifTrue: [
            Smalltalk at: #SYSLOG_NAME put: nil.
        ].
    ]

    print: aMessage [
        <category: 'output'>
        | level |
        level := self class logLevelMap at: aMessage level.
        self class c_syslog: level fmt: '%s%s' args: {self prefix. aMessage msg}.
    ]

    exception: aMessage [
        | level |
        level := self class logLevelMap at: aMessage level.
        self class c_syslog: level fmt: '%s%s'
                    args: {self prefix. 'EXCEPTION occured'}.
        self print: aMessage.
    ]

    prefix: aMsg [
        prefix := aMsg.
    ]

    prefix [
        ^ prefix ifNil: ['']
    ]
]

Eval [
    LogTargetSyslog initialize.
]

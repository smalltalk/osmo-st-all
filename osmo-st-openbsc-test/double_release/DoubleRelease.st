"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: #FakeBTS.

FakeBTS.RSLEstablishRequest extend [
    trxDispatchOn: aTrx with: lchan [
	| msg |
        <category: '*-BTS-Core'>

        'Got an establish request.' printNl.
        msg := FakeBTS.RSLConnectionFailure new
                channelNumber: self channelNumber;
                cause: #[1] asRSLAttributeData;
                yourself.
        aTrx mainBts sendRSL: msg toMessage on: aTrx.

        msg := FakeBTS.RSLReleaseIndication new
                channelNumber: self channelNumber;
                linkIdentifier: self linkIdentifier;
                yourself.    
        aTrx mainBts sendRSL: msg toMessage on: aTrx.

        msg := FakeBTS.RSLErrorIndication new
                channelNumber: self channelNumber;
                linkIdentifier: self linkIdentifier;
                rlmCause: #[1] asRSLAttributeData;
                yourself.
        aTrx mainBts sendRSL: msg toMessage on: aTrx.
    ]

    trxDispatchOn: aTrx [
	<category: '*-BTS-Core'>
	self trxChannelDispatch: aTrx.
    ]
]

FakeBTS.OpenBSCTest subclass: DoubleRelease [
    | tmsi |

    IMSI := '901010000001111'.

    startTest [
	self createAndConnectBTS: '1234/0/0'.

	bts onPaging: [:id | self handlePaging: id].
	tmsi := self allocateTmsi: IMSI.
    ]

    handlePaging: id [
	"Handle paging for TMSI2"
	id tmsi = tmsi ifFalse: [^self].

	"Run it on another process"
	[self handlePagingResponse] fork.
    ]

    handlePagingResponse [
	| lchan msg ti |
	"Handle paging response..."
	lchan := self requireAnyChannel.
	msg := GSM48RRPagingResponse new.
	msg mi tmsi: tmsi.
	lchan sendGSM: msg toMessage.

	Semaphore new wait.
    ]
]

Eval [
    DoubleRelease new
	startTest.

    'Waiting to the end' printNl.
    Semaphore new wait.
]

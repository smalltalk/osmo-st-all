"
 (C) 2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: #FakeBTS.

FakeBTS.OpenBSCTest subclass: IMSIDetach [
    <import: OsmoGSM>

    startTest [
        "1. Connect to the BTS"
        self createAndConnectBTS: '1801/0/0'.
        self testIMSIDetach.
    ]

    testIMSIDetach [
        | lchan detach tmsi |

        tmsi := self allocateTmsi: '901010000001111'.

        "2. Get a LCHAN"
        lchan := self requireAnyChannel.

        "3. Send a IMSI Detach"
        detach := GSM48IMSIDetachInd new.
        detach mi tmsi: tmsi.
        lchan sendGSM: detach toMessage.

        "Wait for the channel to be released.."
        [
            | msg |
            "Read all messages until the end on SAPI=0. Ignore SAPI=3"
            "If we send another SAPI=3 Release Indication we get a double
             RF Channel Release from the NITB."
            [
            msg := GSM48MSG decode: lchan nextSapi0Msg readStream.
            (msg isKindOf: GSM48RRChannelRelease)
                ifTrue: [lchan releaseAllSapis. ^true]
            ] on: Exception do: [Transcript nextPutAll: 'GSM decoding error'; nl.].
        ] repeat.
    ]
]

Eval [
    | test |

    test := IMSIDetach new
                startTest;
                stopBts;
                yourself.
]

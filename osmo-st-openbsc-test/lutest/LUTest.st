"
 (C) 2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: #FakeBTS.

FakeBTS.OpenBSCTest subclass: LUTest [
    <import: OsmoGSM>

    startTest [
        | lchan lu msg |
        "1. Connect to the BTS"
        self createAndConnectBTS: '1801/0/0'. 

        "2. Get a LCHAN"
        lchan := self requireAnyChannel.

        "3. Send the LU request" 
        lu := GSM48LURequest new.
        lu lai
            mcc: 1;
            mnc: 1;
            lac: 1.
        lu mi imsi: '901010000001111'.
        lchan sendGSM: lu toMessage.

        "Now deal with what the NITB wants"
        "4.1 Send the IMEI..."
        msg := GSM48MSG decode: lchan nextSapi0Msg readStream.
        (msg isKindOf: GSM48IdentityReq)
            ifFalse: [^self error: 'Wanted identity request'].
        (msg idType isIMEI)
            ifFalse: [^self error: 'Wanted IMEI request'].
        msg := GSM48IdentityResponse new.
        msg mi imei: '6666666666666666'.
        lchan sendGSM: msg toMessage.
        
        "4.2 LU Accept"
        msg := GSM48MSG decode: lchan nextSapi0Msg readStream.
        (msg isKindOf: GSM48LUAccept)
            ifFalse: [^self error: 'LU failed'].
        msg := GSM48TMSIReallocationComplete new.
        lchan sendGSM: msg toMessage.

        "4.3 MM Information for the time. ignore it"
        msg := GSM48MSG decode: lchan nextSapi0Msg readStream.
        (msg isKindOf: GSM48MMInformation)
            ifFalse: [^self error: 'MM Information'].

        "4.4 release.. if we now don't close the LCHAN it will
        remain open for a bit. OpenBSC should and will start the
        approriate timer soon(tm)"
        msg := GSM48MSG decode: lchan nextSapi0Msg readStream.
        (msg isKindOf: GSM48RRChannelRelease)
            ifFalse: [^self error: 'RR Channel Release'].

        "4.5.. be nice... for now and send a disconnect."
        lchan releaseAllSapis.
    ]
]

Eval [
    | test |

    test := LUTest new
                startTest;
                stopBts;
                yourself.
]

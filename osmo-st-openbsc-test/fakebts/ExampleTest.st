"
 (C) 2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

OpenBSCTest subclass: TestLU [
    <import: OsmoGSM>

    createLU [
        <category: 'helper'>
        | lu lai |
        "Create a LU coming from a different network."

        lu := GSM48LURequest new.
        lu mi imsi: '12345567890'.
        lai := lu lai.
        lai mcc: 208.
        lai mnc: 8.
        lai lac: 29701.

        ^ lu
    ]

    createIdentityResponse: aRequest [
        | mi |
        <category: 'helper'>

        mi := GSM48IdentityResponse new.

        "Respond with a IMEI(SV) or IMSI"
        aRequest idType isIMSI
              ifTrue: [mi imsi: '12345567890']
              ifFalse: [mi imei: '000000000000'].
        ^ mi
    ]

    runLu [
        <category: 'test'>
        | lchan stop |
        "Run a simple LU"
        self createAndConnectBTS.
        (Delay forSeconds: 2) wait.

        lchan := self requireAnyChannel.
        lchan sendGSM: self createLU toMessage.

        stop := false.
        [stop] whileFalse: [ | msg gsm |
            msg := lchan nextSapi0Msg.
            gsm := GSM48MSG decode: msg readStream.

            "Now dispatch messages the boring way"
            gsm type = GSM48RRChannelRelease messageType
                ifTrue: [stop := true. lchan releaseAllSapis].

            gsm type = GSM48IdentityReq messageType
                ifTrue: [lchan sendGSM: (self createIdentityResponse: gsm) toMessage].
        ].
    ]
]


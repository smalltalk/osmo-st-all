"
 (C) 2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: #FakeBTS.

FakeBTS.OpenBSCTest subclass: SACCHTimeout [
    <import: OsmoGSM>

    SACCHTimeout class >> cpDataRpData [
        ^ #(
            16r09 16r01 16r35 16r01 16r2A 16r07 16r91 16r44
            16r77 16r58 16r10 16r06 16r50 16r00 16r2B 16r04
            16r04 16r81 16r32 16r24 16r00 16r00 16r80 16r21
            16r03 16r41 16r24 16r32 16r40 16r1F 16r41 16r26
            16r03 16r94 16r7D 16r56 16rA5 16r20 16r28 16rF2
            16rE9 16r2C 16r82 16r82 16rD2 16r22 16r48 16r58
            16r64 16r3E 16r9D 16r47 16r10 16rF5 16r09 16rAA
            16r4E 16r01) asByteArray.
    ]

    startTest [
        "1. Connect to the BTS"
        self createAndConnectBTS: '1801/0/0'.

        self
            testCPTimeoutSacchTimeout.
    ]


    testCPTimeoutSacchTimeout [
        | lchan cm tmsi wait |

        tmsi := self allocateTmsi: '901010000001111'.

        "2. Get a LCHAN"
        lchan := self requireAnyChannel.

	"Prepare the waiting"
	wait := Semaphore new.
        lchan onReleaseReqCB: [:sapi |
            Transcript nextPutAll: 'Releasing'; nl. wait signal].

        "3. Send a CM Service Request "
        cm := GSM48CMServiceReq new.
        cm mi tmsi: tmsi.
        lchan sendGSM: cm toMessage.

	"4. CP-DATA/RP-DATA PDU"
	lchan sendGSM: self class cpDataRpData sapi: 3.

        "Wait for the channel to be released.."
	wait wait; wait.
        Transcript nextPutAll: 'Channel was released.'; nl.
    ]
]

Eval [
    | test |

    test := SACCHTimeout new
                startTest;
                stopBts;
                yourself.
]

"
 (C) 2012-2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: #FakeBTS.

FakeBTS.OpenBSCTest subclass: SMSCPError [
    <import: OsmoGSM>

    SMSCPError class >> cpError [
        ^ #(16rC9 16r10 16r51) asByteArray.
    ]

    startTest [
        "1. Connect to the BTS"
        self createAndConnectBTS: '1801/0/0'.

        self
            testCpError.
    ]

    testCpError [
        | lchan cm tmsi |

        tmsi := self allocateTmsi: '901010000001111'.

        "2. Get a LCHAN"
        lchan := self requireAnyChannel.

        "3. Send a CM Service Request "
        cm := GSM48CMServiceReq new.
        cm mi tmsi: tmsi.
        lchan sendGSM: cm toMessage.

	"4. CP-DATA/RP-DATA PDU"
	lchan sendGSM: self class cpError sapi: 3.

        "Wait for the channel to be released.."
        [
            | msg |
            "Check what happens as a response to this message now..."
            [
                msg := GSM48MSG decode: lchan nextSapi0Msg readStream.
                (msg isKindOf: GSM48RRChannelRelease)
                    ifTrue: [lchan releaseAllSapis. ^true].
                msg inspect.
            ] on: Exception do: [Transcript nextPutAll: 'GSM decoding error'; nl.].
        ] repeat.
    ]
]

Eval [
    | test |

    test := SMSCPError new
                startTest;
                stopBts;
                yourself.
]

# Copyright (C) 2012 Holger Hans Peter Freyther
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import dbus
import sim, sms

class Modem(object):
    def __init__(self, bus, name):
        self.name = name
        self.bus = bus
        self.modem = dbus.Interface(bus.get_object('org.ofono', name), 'org.ofono.Modem')

    def enable(self):
        """
        Enable the given modem on the Bus
        """
        self.modem.SetProperty("Powered", dbus.Boolean(1), timeout = 120)

    def disable(self):
        """
        Enable the given modem on the Bus
        """
        self.modem.SetProperty("Powered", dbus.Boolean(0), timeout = 120)

    def online(self):
        """
        Switch-on on the RF Modem
        """
        self.modem.SetProperty("Online", dbus.Boolean(1), timeout = 120)

    def offline(self):
        """
        Switch-off on the RF Modem
        """
        self.modem.SetProperty("Online", dbus.Boolean(0), timeout = 120)

    def is_enabled(self):
        """
        Is the modem online?
        """
        return bool(self._get_property('Powered'))

    def manufacturer(self):
        """
        Who is the owner of the mode?
        """
        man = self.modem.GetProperties()
        try:
            return str(man['Manufacturer'])
        except:
            return None

    def _get_property(self, name):
        """
        Internal
        """
        return self.modem.GetProperties()[name]

    def sim(self):
        return sim.Sim(self.bus, self.name)

    def sms(self):
        return sms.SmsManager(self.bus, self.name)

    def register(self):
        """Ask for the module to register"""
        network = dbus.Interface(
                    self.bus.get_object('org.ofono', self.name),
                    'org.ofono.NetworkRegistration')
        network.Register()

    def __repr__(self):
        return "<Modem('%s')>" % self.name


def get(bus, name):
    """
    Find the modem 
    """
    return Modem(bus, name)

def getmodems(bus):
    """
    Find modems...
    """
    obj = dbus.Interface(bus.get_object('org.ofono', '/'), 'org.ofono.Manager')
    return [Modem(bus, str(x[0])) for x in obj.GetModems()]

def detect_modems(bus, sleep=True, poweroff=True):
    """
    Detect the modems that can be used for the test...
    """
    modems = getmodems(bus)

    # Filter out the phonesim
    modems = filter(lambda x: x.name != '/phonesim', modems)

    wait = []
    on = []

    # Enable each modem...
    for mod in modems:
        if mod.is_enabled():
            on.append(mod)
        else:
            print("Going to enable modem: %s" % mod.name)
            mod.enable()
            wait.append(mod)

    # Now... wait a bit for the modem to do some init
    if len(wait) >0 and sleep:
        import time
        print("I need to sleep some time for the modem to wake up")
        time.sleep(20)

        for mod in wait:
            if mod.is_enabled():
                on.append(mod)

    # Now filter out the modems without a SIM Card
    def modem_vendor(modem):
        # Check if the modem vendor was queried
        return modem.manufacturer() != None

    def sim_present(modem):
        return modem.sim().imsi() != None

    on = filter(modem_vendor, on)
    on = filter(sim_present, on)

    # TODO: We could now disable all modems without a SIMcard
    for mod in modems:
        if mod in on or not poweroff:
            continue
        print("Modem %s is wihtout SIM card. Powering it down." % mod.name)
        mod.disable()

    return on


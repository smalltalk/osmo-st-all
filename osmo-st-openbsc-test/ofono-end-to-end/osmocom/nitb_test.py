# Copyright (C) 2012 Holger Hans Peter Freyther
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import dbus, modem, openbsc

class NitbTest(object):
    """
    I help with testing NITB/pseudoMSC code. E.g. for call testing..
    and SMS..
    """
    def __init__(self, host):
        self.host = host
        self.socket = openbsc.NITBSocket(host)

    def setup(self):
        # Get the modems.. now check which ones can be used for the
        # test with a proper IMSI
        self.modems = modem.detect_modems(dbus.SystemBus())
        self.avail_modems = []
        self.ext2mod = {}
        self.mod2ext = {}

        for mod in self.modems:
            imsi = mod.sim().imsi()

            if not self.socket.imsi_present(imsi):
                print("Modem('%s') doesn't have a provisioned SIM('%s')" % (mod.name, imsi))
                continue

            self.avail_modems.append(mod)
            ext = self.socket.extension(imsi)
            self.ext2mod[ext] = mod
            self.mod2ext[mod] = ext

            # Now register
            try:
                mod.register()
            except:
                print("Registering %s failed. Continuing anyway" % mod)

        # TODO: Check if all modems are registered? But this would delay the
        # test further. But the modem's SIM card is inside the NITB HLR so it
        # should be able to register.

    def teardown(self):
        pass

    def run(self):
        self.setup()
        self.run_test()
        self.teardown()

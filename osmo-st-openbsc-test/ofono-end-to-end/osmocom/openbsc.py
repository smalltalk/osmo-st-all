# Copyright (C) 2012 Holger Hans Peter Freyther
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# VTY helper code for OpenBSC
#
import socket

class _VTYSocket(object):
    def __init__(self, name, host, port):
        self.name = name
        self.host = host
        self.port = port

    def connectSendAndWait(self, request):
	sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sck.setblocking(1)
	sck.connect((self.host, self.port))
	sck.recv(4096)

        end = '\r\n%s> ' % self.name

	# Now send the command
	sck.send("%s\r" % request)
	res = ""
	while True:
		data = sck.recv(4096)
		res = "%s%s" % (res, data)
		if res.endswith(end):
			break
        sck.close()
        return res[len(request) + 2: -len(end)]

class NITBSocket(object):
    def __init__(self, host):
        self.host = host

    def _vty(self):
        return _VTYSocket('OpenBSC', self.host, 4242)

    def imsi_present(self, imsi):
        res = self._vty().connectSendAndWait('show subscriber imsi %s' % imsi)
        return not res.startswith('% No subscriber found for imsi ')

    def extension(self, imsi):
        if not self.imsi_present(imsi):
            return None
        res = self._vty().connectSendAndWait('show subscriber imsi %s' % imsi)
        return res.split('\r\n')[2].split(': ')[1]

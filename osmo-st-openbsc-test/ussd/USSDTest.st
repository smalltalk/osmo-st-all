"
 (C) 2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: #FakeBTS.

FakeBTS.OpenBSCTest subclass: USSDTest [
    <import: OsmoGSM>

    USSDTest class >> registerMessage [
        ^#[16r1B 16r3B 16r1C 16r15 16rA1 16r13 16r02
           16r01 16r04 16r02 16r01 16r3B 16r30 16r0B
           16r04 16r01 16r0F 16r04 16r06 16rAA 16r51
           16r0C 16r06 16r1B 16r01 16r7F 16r01 16r00]
    ]

    USSDTest class >> interrogateSS [
	^#[16r1B 16r7B 16r1C 16r0D 16rA1 16r0B 16r02
	   16r01 16r01 16r02 16r01 16r0E 16r30 16r03
	   16r04 16r01 16r11 16r7F 16r01 16r00]
    ]

    startTest [
        "1. Connect to the BTS"
"        bts := self createBTS.
        bts btsId: '106/0/0'.
        bts connect: '192.168.46.1'.
        bts waitForBTSReady."
        self createAndConnectBTS: '1234/0/0'.

        self
            testUSSDMessage
    ]

    testUSSDMessage [
        | tmsi cm lchan |

        tmsi := self allocateTmsi: '901010000001111'.

        "2. Get a LCHAN"
        lchan := self requireAnyChannel.

        "3. Send a CM Service Request "
        cm := GSM48CMServiceReq new.
        cm mi tmsi: tmsi.
        lchan sendGSM: cm toMessage.

	"4. USSD messages"
	lchan sendGSM: self class interrogateSS sapi: 3.

        stdin next.
    ]
]

Eval [
    | test |

    test := USSDTest new
                startTest;
                stopBts;
                yourself.
]

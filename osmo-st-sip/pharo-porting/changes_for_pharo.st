
SIPURandom class extend [
    nextByte [
        <category: 'random'>
        | file |
	"Pharo has a weird kind of stream support"

        file := (FileStream readOnlyFileNamed: '/dev/urandom')
		    binary; yourself.
        [
            ^ file next value.
        ] ensure: [
            file close.
        ]
    ]

    nextInt [
        <category: 'random'>
        | file |
        file := (FileStream readOnlyFileNamed: '/dev/urandom')
		    binary; yourself.
        [
            | data |
            data := ByteArray new: 4.
            1 to: data size do: [:each |
                data at: each put: file next value.
            ].

            ^ data uintAt: 1
        ] ensure: [
            file close.
        ]
    ]
]

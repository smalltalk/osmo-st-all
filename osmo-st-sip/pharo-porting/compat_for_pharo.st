Object subclass: GSTSocketAddress [
    <category: 'OsmoSIP-Pharo'>
    <comment: 'TODO: Move it into the OsmoNetwork package'>

    GSTSocketAddress class >> localHostName [
	^ NetNameResolver localHostName
    ]
]

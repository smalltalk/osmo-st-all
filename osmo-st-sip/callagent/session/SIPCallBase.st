"
 (C) 2011, 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPSessionBase subclass: SIPCallBase [
    | state |

    <category: 'OsmoSIP-Callagent'>
    <comment: 'I am the base class for incoming and outgoing 
sessions (calls). The common code goes here'>

    SIPCallBase class >> stateInitial [ <category: 'states'> ^ #initial ]
    SIPCallBase class >> stateInvite  [ <category: 'states'> ^ #invite  ]
    SIPCallBase class >> stateSession [ <category: 'states'> ^ #session ]
    SIPCallBase class >> stateTimeout [ <category: 'states'> ^ #timeout ]
    SIPCallBase class >> stateCancel  [ <category: 'states'> ^ #cancel  ]
    SIPCallBase class >> stateHangup  [ <category: 'states'> ^ #hangup  ]
    SIPCallBase class >> stateRemoteHangup [ <category: 'states'> ^ #remoteHangup ]
    SIPCallBase class >> stateFailed  [ <category: 'states'> ^ #failed ]
    SIPCallBase class >> stateRedirect [ <category: 'states'> ^ #redirect ]

    state [
        <category: 'states'>
        ^ state ifNil: [self class stateInitial]
    ]

    moveToState: aState [
        <category: 'states'>

        "E.g. we get two remote hang ups as our ack has not arrived."
        self state = aState ifTrue: [^true].

        "Check if there is a state transition"
        (self class legalStates includes: (self state -> aState)) ifFalse: [
            self logError: ('SIPCallBase(<1s>) transition <2p>-><3p> is not legal.'
                    expandMacrosWithArguments: {self callId. self state. aState})
                        area: #sip.
            ^ false
        ].

        state := aState.
        ^ true
    ]

    hangup [
        <category: 'call'>
        (self moveToState: self class stateHangup) ifTrue: [
            self logNotice: ('SIPCallBase(<1s>) going to hangup.' expandMacrosWith: self callId)
                    area: #sip.

            self unregisterDialog.
            (SIPByeTransaction createWith: dialog on: ua cseq: self nextCSeq)
                onTimeout: [self hangupTimedOut];
                onSuccess: [:resp :dlg | self hangupSuccess: resp dialog: dlg];
                onFailure: [:resp :dlg | self hangupFailure: resp dialog: dlg];
                start
        ].
    ]

    hangupTimedOut [
        <category: 'hangup-result'>
        self logNotice: ('SIPCallBase(<1s>) hang-up timedout.'
                    expandMacrosWith: self callId) area: #sip.
        self sessionEnd.
    ]

    hangupSuccess: aResponse dialog: aDialog [
        <category: 'hangup-result'>
        (self check: aDialog) ifFalse: [
            self logError: ('SIPCallBase(<1s>) can only have one session. Ignoring failure.'
                    expandMacrosWith: self callId) area: #sip.
            ^ false
        ].

        self logNotice: ('SIPCallBase(<1s>) hang-up success.'
                    expandMacrosWith: self callId) area: #sip.
        self sessionEnd.
    ]

    hangupFailure: aResponse dialog: aDialog [
        <category: 'hangup-result'>
        (self check: aDialog) ifFalse: [
            self logError: ('SIPCallBase(<1s>) can only have one session. Ignoring failure.'
                    expandMacrosWith: self callId) area: #sip.
            ^ false
        ].

        self logNotice: ('SIPCallBase(<1s>) hang-up failure.'
                    expandMacrosWith: self callId) area: #sip.
        self sessionEnd.
    ]

    remoteHangup: aRequest dialog: aDialog [
        <category: 'remote-hangup'>

        (self moveToState:  self class stateRemoteHangup) ifTrue: [
            self logNotice: ('SIPCallBase(<1s>) session remotely terminated.'
                    expandMacrosWith: self callId) area: #sip.
            ua respondWith: 200 phrase: 'OK' on: aRequest dialog: aDialog.
            self unregisterDialog.
            self sessionEnd.
        ].
    ]

    newRequest: aRequest dialog: aDialog [
        <category: 'private'>
        aRequest sipCallDispatch: self dialog: aDialog.
    ]

    sessionNew [
        <category: 'callback'>
        "Add custom handling"
    ]

    sessionFailed [
        <category: 'callback'>
        "Add custom handling"
    ]

    sessionEnd [
        <category: 'callback'>
        "Add custom handling"
    ]

]

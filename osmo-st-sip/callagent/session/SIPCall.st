"
 (C) 2011, 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPCallBase subclass: SIPCall [
    | sdp_offer sdp_result invite |
    <category: 'OsmoSIP-Callagent'>
    <comment: 'I am a high level class to deal with transactions,
sessions and calls. Right now I do not support forking proxies and
will simply ignore everything but the first dialog.'>


    LegalStates := nil.

    SIPCall class >> legalStates [
        <category: 'states'>
        ^ LegalStates ifNil: [
            LegalStates := {
                self stateInitial -> self stateInvite.
                self stateInvite  -> self stateSession.
                self stateInvite  -> self stateTimeout.
                self stateInvite  -> self stateCancel.
                self stateInvite  -> self stateFailed.
                self stateInvite  -> self stateRedirect.
                self stateSession -> self stateHangup.
                self stateSession -> self stateRemoteHangup.
                self stateHangup  -> self stateRemoteHangup.
            }
        ]
    ]

    SIPCall class >> fromIdenity: aUser identity: anIdentity host: aHost port: aPort to: aTo on: aUseragent [
        "Remove in a year! 20150620"
        ^self fromIdentity: aUser identity: anIdentity host: aHost port: aPort to: aTo on: aUseragent
    ]

    SIPCall class >> fromIdentity: aUser identity: anIdentity host: aHost port: aPort to: aTo on: aUseragent [
        ^ self
            on: ((SIPDialog fromUser: aUser host: aHost port: aPort)
                    to: aTo; identity: anIdentity; yourself) useragent: aUseragent
    ]

    SIPCall class >> fromUser: aUser host: aHost port: aPort to: aTo on: aUseragent [
        ^self fromIdenity: aUser identity: aUseragent mainIdentity host: aHost port: aPort to: aTo on: aUseragent
    ]

    createCall: aSDPOffer [
        <category: 'call'>

        (self moveToState: self class stateInvite) ifFalse: [
            self logError: ('SIPCall(<1s>) failed to start.' expandMacrosWith: self callId)
                    area: #sip.
            ^ false
        ].


        sdp_offer := aSDPOffer.

        next_cseq := initial_dialog cseq + 1.
        invite := (SIPInviteTransaction createWith: initial_dialog on: ua with: aSDPOffer cseq: initial_dialog cseq)
                    onTimeout: [self callTimedOut];
                    onSuccess: [:response :dialog | self callSuccess: response dialog: dialog];
                    onFailure: [:response :dialog | self callFailure: response dialog: dialog];
                    onNotification: [:response :dialog | self callNotification: response dialog: dialog];
                    start;
                    yourself
    ]

    cancel [
        <category: 'call'>
        (self moveToState: self class stateCancel) ifTrue: [
            self logNotice: ('SIPCall(<1s>) going to cancel it.' expandMacrosWith: self callId)
                    area: #sip.

            self unregisterDialog.
            invite cancel.
            ^ true
        ].

        ^ false
    ]

    callTimedOut [
        <category: 'call-result'>
        self logError: ('SIPCall(<1s>) timed-out.' expandMacrosWith: self callId)
                area: #sip.

        (self moveToState: self class stateTimeout) ifFalse: [
            invite := nil.
            ^ self logError: ('SIPCall(<1s>) failed to move to timeout.'
                                expandMacrosWith: self callId) area: #sip.
        ].
    ]

    callSuccess: aResponse dialog: aDialog [
        <category: 'call-result'>
        (self check: aDialog) ifFalse: [
            self logError: ('SIPCall(<1s>) can only have one session. Ignoring.'
                    expandMacrosWith: self callId) area: #sip.
            ^ false
        ].


        (self moveToState: self class stateSession) ifTrue: [
            self logNotice: ('SIPCall(<1s>) session established.'
                    expandMacrosWith: self callId) area: #sip.
            sdp_result := aResponse sdp.
            self sessionNew.
            invite := nil.
            ^ true
        ].
        ^ false
    ]

    callFailure: aResponse dialog: aDialog [
        <category: 'call-result'>
        (self check: aDialog) ifFalse: [
            self logError: ('SIPCall(<1s>) can only have one session. Ignoring failure.'
                    expandMacrosWith: self callId) area: #sip.
            ^ false
        ].

        "Check if that is a redirect"
        (self handleRedirect: aResponse dialog: aDialog) ifTrue: [
            ^self
        ].

        (self moveToState: self class stateFailed) ifTrue: [
            invite := nil.
            self logNotice: ('SIPCall(<1s>) call failure.'
                    expandMacrosWith: self callId) area: #sip.
            self sessionFailed.
        ].
    ]

    callNotification: aResponse dialog: aDialog [
        <category: 'call-result'>
        (self check: aDialog) ifFalse: [
            self logError: ('SIPCall(<1s>) can only have one session. Ignoring notification.'
                    expandMacrosWith: self callId) area: #sip.
            ^ false
        ].

        self logNotice: ('SIPCall(<1s>) notification.'
                    expandMacrosWith: self callId) area: #sip.
        self sessionNotification: aResponse.
    ]

    remoteReInvite: aRequest dialog: aDialog [
        "TODO: check if we are in a session..."
        ua respondWith: 200 phrase: 'OK' on: aRequest dialog: aDialog.
    ]

    terminate [
        <category: 'public'>
        "I try to finish up things."
        self state = self class stateInvite ifTrue: [^self cancel].
        self state = self class stateSession ifTrue: [^self hangup].

        self logError: ('SIPCall(<1s>) terminate not possible in <2p>.'
                    expandMacrosWithArguments: {self callId. self state})
                        area: #sip.
    ]

    handleRedirect: aResponse dialog: aDialog [
        | code |
        code := aResponse code asInteger.
        (code = 301 or: [code = 302])
            ifFalse: [^false].

        (self moveToState: self class stateRedirect) ifTrue: [
            invite := nil.
            self logNotice: ('SIPCall(<1s>) call redirect.'
                    expandMacrosWith: self callId) area: #sip.
            self sessionRedirect: (self extractContact: aResponse).
        ].

        ^true.
    ]

    sessionRedirect: aContact [
        <category: 'callback'>
        "Legacy support to just fail"
        ^self sessionFailed
    ]

    sessionNotification: aNot [
        <category: 'callback'>
    ]

    extractContact: aResponse [
        | contact |
        contact := aResponse parameter: 'Contact'.
        contact ifNil: [^nil].

        ^contact first
    ]

    sdpResult [
        ^sdp_result
    ]

    sessionAcked: anAck dialog: aDialog [
	"Do nothing. E.g. this could have been a re-invite that we 200 OKed
	and now get an ACK for our 200. In the future we might want to handle
	the re-invitation differently."
    ]
]

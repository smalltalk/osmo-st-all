"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SIPSessionBase [
    | rem ua initial_dialog dialog next_cseq |
    <category: 'OsmoSIP-Callagent'>
    <comment: 'I am the base for sessions. I am a bit backward as the
Dialog will create/hold the session but we start with the session here
as this is what we are really interested in. So this is not really a
session as of the RFC... but at some stage in the exchange we will be
a proper session.'>

    SIPSessionBase class >> on: aDialog useragent: aUseragent [
        <category: 'creation'>
        ^ self new
            useragent: aUseragent;
            initialDialog: aDialog;
            yourself
    ]

    initialDialog: aDialog [
        <category: 'creation'>
        initial_dialog := aDialog.
        initial_dialog contact: ('sip:<1s>@<2p>:<3p>'
                expandMacrosWith: aDialog identity username with: ua transport address with: ua transport port).
    ]

    useragent: aUseragent [
        <category: 'creation'>
        ua := aUseragent
    ]

    callId [
        <category: 'info'>
        ^ initial_dialog callId
    ]

    check: aDialog [
        <category: 'private'>
        "I check if this enters a new confirmed dialog or if this is the
        confirmed dialog."

        "We have now a confirmed dialog, accept it"
        ^ dialog isNil
            ifTrue: [
                aDialog isConfirmed ifTrue: [
                    dialog := aDialog.
                    self registerDialog.
                    self logNotice: ('SIPCall(<1s>) dialog is confirmed now.'
                            expandMacrosWith: self callId) area: #sip.
                ].
                true]
            ifFalse: [
                "We could fork things here. For multi party call"
                dialog to_tag = aDialog to_tag].
    ]

    registerDialog [
        <category: 'session'>
        ua registerDialog: self.
    ]

    unregisterDialogIsPending [
        ^rem isNil not
    ]

    unregisterDialog [
        <category: 'session'>
        rem ifNil: [
            rem := Osmo.TimerScheduler instance
                    scheduleInSeconds: 60 block: [
                        ua unregisterDialog: self.
                    ]]
    ]

    nextCSeq [
        | res |
        <category: 'accessing'>
        res := next_cseq.
        next_cseq := next_cseq + 1.
        ^ res
    ]

    isCompatible: aDialog [
        <category: 'dialog'>
        ^ dialog isNil
                    ifTrue:  [initial_dialog isCompatible: aDialog]
                    ifFalse: [dialog isCompatible: aDialog].
    ]

    newRequest: aRequest dialog: aDialog [
        <category: 'dialog'>
        self logError: ('<1p>(<2s>) unhandled request <3p>.'
                expandMacrosWithArguments: {self class. self callId. aRequest class verb})
                     area: #sip.
    ]
]

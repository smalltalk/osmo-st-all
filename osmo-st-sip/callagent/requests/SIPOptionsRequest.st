"
 (C) 2010-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPRequest subclass: SIPOptionsRequest [
    <category: 'OsmoSIP-Callagent'>

    SIPOptionsRequest class >> verb [
        <category: 'verb'>
        ^ 'OPTIONS'
    ]

    addDefaults: out [
        super addDefaults: out.

        "Add a contact if we have a dialog"
        dialog isNil ifFalse:[
          self parameter: 'Contact' ifAbsent: [
              out
                    nextPutAll: 'Contact: <';
                    nextPutAll: dialog contact;
                    nextPutAll: '>'; cr; nl]].

        self parameter: 'Accept' ifAbsent: [
            out nextPutAll: 'Accept: application/sdp'; cr; nl.
        ].
    ]
]

"
 (C) 2011-2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPRequest extend [
    sipDispatchNewDialog: aDialog on: aUserAgent [
        <category: 'OsmoSIP-Callagent'>
        self logError: 'Unknown action for ', self class name area: #sip.
    ]
]

SIPByeRequest extend [
    sipDispatchNewDialog: aDialog on: aUserAgent [
        <category: 'OsmoSIP-Callagent'>
        self logNotice: 'Unknown call ', self class name area: #sip.
        aUserAgent
            respondWith: 481
            phrase: 'Call/Transaction Does Not Existing'
            on: self
            dialog: aDialog.
    ]
]

SIPOptionsRequest extend [
    sipDispatchNewDialog: aDialog on: aUserAgent [
        <category: 'OsmoSIP-Callagent'>
        aUserAgent
            respondWith: 481
            phrase: 'Call Leg/Transaction Does Not Exist'
            on: self
            dialog: aDialog.
    ]
]

SIPInviteRequest extend [
    sipDispatchNewDialog: aDialog on: aUserAgent [
        <category: 'OsmoSIP-Callagent'>
        aUserAgent
            newInvite: self
            dialog: aDialog.
    ]
]

"
 (C) 2011-2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPUserAgentBase subclass: SIPUserAgent [
    | dialogs transactions retransmit sem parser mainIdentity onNewCall |
    <category: 'OsmoSIP-Callagent'>
    <comment: 'I am a user agent base'>

    SIPUserAgent class >> new [
        <category: 'creation'>
        ^self basicNew initialize
    ]

    initialize [
        <category: 'creation'>
        sem := Semaphore forMutualExclusion.
        mainIdentity := SIPIdentity new.
    ]

    dialogs [
        <category: 'private'>
        ^ dialogs ifNil: [dialogs := OrderedCollection new]
    ]

    registerDialog: aDialog [
        <category: 'dialogs'>
        sem critical: [
            self dialogs add: aDialog]
    ]

    unregisterDialog: aDialog [
        <category: 'dialogs'>
        sem critical: [
            self dialogs remove: aDialog ifAbsent: [
                self logError: ('<1p> dialog <2p> is not present.' expandMacrosWithArguments:
                    {self. aDialog}) area: #sip]]
    ]

    transactions [
        <category: 'private'>
        ^ transactions ifNil: [transactions := OrderedCollection new]
    ]

    addTransaction: aTransaction [
        <category: 'transactions'>
        sem critical: [
            self transactions add: aTransaction]
    ]

    removeTransaction: aTransaction [
        <category: 'transactions'>
        sem critical: [
            self transactions remove: aTransaction ifAbsent: [
                self logError: ('<1p> transaction <2p> is not present.'
                        expandMacrosWithArguments: {self. aTransaction}) area: #sip.]].
    ]

    dispatchRequest: aReq destIp: aDestIp destPort: aDestPort [
        | dialogs dialog |
        <category: 'dispatch'>
        dialog := (SIPDialog fromMessage: aReq)
                    destIp: aDestIp;
                    destPort: aDestPort;
                    yourself.

        dialogs := sem critical: [self dialogs copy].
        dialogs do: [:each |
            (each isCompatible: dialog) ifTrue: [
                each newRequest: aReq dialog: dialog.
                ^ self
            ]].

        self newDialog: dialog request: aReq.
    ]

    dispatchResponse: aReq [
        | branch trans |
        <category: 'dispatch'>

        branch := (aReq parameter: 'Via') branch.

        "Check if the new RFC is implemented"
        (branch copyFrom: 1 to: 7) = self class branchStart ifFalse: [
            self logError: 'Branch does not start with magic cookie' area: #sip.
            ^ false
        ].

        trans := sem critical: [self transactions copy].
        trans do: [:each |
            each branch = branch ifTrue: [
                ^each newData: aReq.
            ].
        ].

        self logNotice: ('Unhandled branch <1s>' expandMacrosWith: branch) area: #sip.
    ]

    transportData: aTransport data: aData [
        <category: 'private'>
        OsmoDispatcher dispatchBlock: [self transportDataInternal: aData].
    ]

    parser [
        <category: 'parser'>
        ^ parser ifNil: [parser := SIPParser new]
    ]

    transportDataInternal: aDatagram [
        <category: 'private'>

        [
            | data |
            data := aDatagram data copyFrom: 1 to: aDatagram size.
            self
                dispatchInternal: data
                ip: aDatagram address displayString
                port: aDatagram port.
        ] on: Error do: [:e |
            e logException: ('Parsing error <1p>' expandMacrosWith: e tag) area: #sip.
        ]
    ]

    dispatchInternal: aByteArray ip: anIp port: aPort [
        | req |
        req := self parser parse: aByteArray asString onError: [:e |
            aByteArray printNl.
            self logError: ('Failed to parse: "<1p>" with <2p>'
                                expandMacrosWithArguments: {aByteArray asString. e}) area: #sip.
            ^ false
        ].

        req isRequest
            ifTrue:  [self dispatchRequest: req destIp: anIp destPort: aPort]
            ifFalse: [self dispatchResponse: req].
    ]

    respondWith: aCode phrase: aPhrase on: aRequest dialog: dialog [
        | resp via cseq |
        <category: 'Sending'>

        via := aRequest parameter: 'Via' ifAbsent: [^ self].
        cseq := aRequest parameter: 'CSeq' ifAbsent: [^ self].

        resp := (SIPResponse code: aCode with: aPhrase)
                    addParameter: 'Via' value: (self generateVia: via branch);
                    addParameter: 'From' value: dialog generateFrom;
                    addParameter: 'To' value: dialog generateTo;
                    addParameter: 'Call-ID' value: dialog callId;
                    addParameter: 'CSeq' value: ('<1p> <2s>'
                                            expandMacrosWith: cseq number with: cseq method);
                    addParameter: 'Allow' value: 'ACK,BYE';
                    yourself.
        self injectDefaults: resp.
        self queueData: resp asDatagram dialog: dialog.
    ]

    newDialog: aDialog request: aRequest [
        <category: 'We have a new dialog'>
        "This might be a re-transmit of a BYE. So we will use the
        double dispatch to check what we should do with this dialog."

        aRequest sipDispatchNewDialog: aDialog on: self.
    ]

    username: aUser [
        <category: 'accessing'>
        mainIdentity username: aUser.
    ]

    password: aPass [
        <category: 'accessing'>
        mainIdentity password: aPass
    ]

    mainIdentity [
        ^mainIdentity
    ]

    onNewCall: aBlock [
        <category: 'incoming-call'>
        onNewCall := aBlock
    ]

    newInvite: anInvite dialog: aDialog [
        <category: 'incoming-call'>

        "We have a new INVITE. Ask the upper layer to do something about
        it. If there is no upper layer just reject the call."
        onNewCall isNil
            ifTrue: [self respondWith: 603 phrase: 'Not Found' on: anInvite dialog: aDialog]
            ifFalse: [onNewCall value: anInvite value: aDialog].
    ]
]

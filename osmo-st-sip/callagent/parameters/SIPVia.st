"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPParam subclass: SIPVia [
    | branch address port |

    <category: 'OsmoSIP-Param'>

    SIPVia class >> findBranch: aData [
        <category: 'creation'>
        aData do: [:each |
            each second key = 'branch' ifTrue: [
                ^ each second value
            ]].
        ^ nil.
    ]

    SIPVia class >> parseFrom: aParseDict [
        | port |
        <category: 'creation'>
        (aParseDict at: 7) second
            ifNotNil: [:val | port := val second asInteger].
        ^self basicNew
            data: aParseDict asFoldedString;
            address: (aParseDict at: 7) first;
            port: port;
            branch: (self findBranch: (aParseDict at: 8));
            yourself
    ]

    data: aData [
        data := aData
    ]

    address: anAddress [
        address := anAddress
    ]

    port: aPort [
        port := aPort
    ]

    branch: aBranch [
        branch := aBranch
    ]

    branch [
        ^ branch
    ]

    address [
        ^address
    ]

    port [
        ^port ifNil: [5060]
    ]
]

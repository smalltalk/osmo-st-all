"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPParam subclass: SIPToFromParam [
    | addr params |

    <category: 'OsmoSIP-Param'>
    <comment: 'I represent a To/From parameter'>

    SIPToFromParam class >> buildParams: aParam [
        <category: 'creation'>
        ^ aParam inject: Dictionary new into: [:dict :each |
                dict at: (each second key) put: (each second);
                     yourself].
    ]

    SIPToFromParam class >> parseFrom: anArray [
        <category: 'creation'>
        ^ self basicNew
            address: anArray first third; 
            data: anArray asFoldedString;
            params: (self buildParams: anArray second);
            yourself
    ]

    address: anAddress [
        addr := anAddress
    ]

    data: aData [
        data := aData
    ]

    params: aParam [
        params := aParam
    ]

    address [
        <category: 'accessing'>
        ^ addr
    ]

    tag [
        | res |
        <category: 'accessing'>
        res := params at: 'tag' ifAbsent: [^nil].
        ^ res value.
    ]

    valueAt: aKey [
        ^ (params at: aKey) value.
    ]
]

"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SIPGenericParam [
    | key value |

    <category: 'OsmoSIP-Param'>
    <comment: 'I am a simple key value pair with the value being optional'>

    SIPGenericParam class >> fromOptional: anArray [
        "The =value is optional here"
        <category: 'creation'>
        ^ self new
            key: anArray first;
            value: (anArray second isNil
                        ifTrue: [nil]
                        ifFalse: [anArray second second])
    ]

    SIPGenericParam class >> fromMandatory: anArray [
        "There must be a key and value"
        <category: 'creation'>
        ^ self new
            key: anArray first;
            value: anArray third.
    ]

    key: aKey [
        <category: 'accessing'>
        key := aKey.
    ]

    value: aValue [
        <category: 'accessing'>
        value := aValue.
    ]

    key [
        <category: 'accessing'>
        ^ key
    ]

    value [
        <category: 'accessing'>
        ^ value
    ]

    asFoldedString [
        | str |
        <category: 'helper'>
        ^value isNil
            ifTrue: [key]
            ifFalse: [
                (WriteStream on: (String new: key size + value size + 1))
                    nextPutAll: key;
                    nextPut: $=;
                    nextPutAll: value;
                    contents].
    ]

    isGenericSIPParam [
        <category: 'osmo-sip-extension'>
        ^ true
    ]
]

"
 (C) 2011,2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPTransaction subclass: SIPRegisterTransaction [
    | destination |
    <category: 'OsmoSIP-Callagent-Transactions'>

    SIPRegisterTransaction class >> operationName [
        ^SIPRegisterRequest verb
    ]

    destination: aDestination [
        destination := aDestination.
    ]

    start [
        ^super start.
    ]

    createRegister: aDialog [
        | reg |

        "We address ourselves with the register"
        aDialog to: aDialog from.

        reg := (SIPRegisterRequest from: aDialog)
                destination: destination;
                addParameter: 'Via' value: (useragent generateVia: branch);
                addParameter: 'CSeq' value: ('<1p> <2s>'
                        expandMacrosWith: cseq with: 'REGISTER');
                addParameter: 'Call-ID' value: aDialog callId;
                addParameter: 'Contact' value: '<', aDialog contact, '>';
                addParameter: 'Expires' value: '3600';
                yourself.
        self addAuthorizationTo: reg.
        useragent injectDefaults: reg.
        ^reg
    ]

    transmit [
        | register |
        register := self createRegister: initial_dialog.
        self queueData: register asDatagram dialog: initial_dialog.
    ]
]

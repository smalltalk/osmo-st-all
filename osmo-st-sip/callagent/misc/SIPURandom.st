"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SIPURandom [
    <category: 'OsmoSIP-Misc'>
    <comment: 'I am using urandom for random numbers. In case of of
    enough data I am not going to block but that means that these are
    not cryptographically strong numbers'>

    SIPURandom class >> nextByte [
        <category: 'random'>
        | file |
        file := FileDescriptor open: '/dev/urandom' mode: 'r'.
        [
            ^ file next value.
        ] ensure: [
            file close.
        ]
    ]

    SIPURandom class >> nextFourBytes [
        | file |
        file := FileDescriptor open: '/dev/urandom' mode: 'r'.
        [
            | data |
            data := ByteArray new: 4.
            1 to: data size do: [:each |
                data at: each put: file next value.
            ].

            ^data
        ] ensure: [
            file close.
        ]
    ]

    SIPURandom class >> nextInt [
        <category: 'random'>
        ^self nextFourBytes uintAt: 1
    ]

    SIPURandom class >> newClientNonce [
        ^self nextFourBytes hex
    ]
]

"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SIPRandomHelper [
    <category: 'OsmoSIP-Misc'>
    <comment: 'I help with generating numbers'>

    SIPRandomHelper class >> generateTag [
        <category: 'random'>
        ^ SIPBase64 encode: ('<1p><2p' expandMacrosWithArguments:
            {DateTime now asSeconds. SIPURandom nextInt})
    ]

    SIPRandomHelper class >> generateCallId [
        ^ '<1s>@<2s>' expandMacrosWithArguments: {
            SIPBase64 encode: SIPURandom nextInt asString.
            Sockets.SocketAddress localHostName.}
    ]
]

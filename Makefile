
.PHONY = osmo-st-asn1.star osmo-st-core.star osmo-st-gsm.star osmo-st-logging.star \
	 osmo-st-mgcp.star osmo-st-msc.star osmo-st-network.star osmo-st-sip.star \
	 osmo-st-testphone.star petitparser.star petitparser-tests.star soapopera.star \
	 osmo-st-openbsc-test.star iliad grease.star

GST_PACKAGE = gst-package
GST_PACKAGE_ARGS = 

%.star:
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) `basename $@ .star`/package.xml

osmo-st-openbsc-test.star:
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) osmo-st-openbsc-test/fakebts/package.xml

all:

install: osmo-st-core.star osmo-st-asn1.star osmo-st-gsm.star osmo-st-logging.star \
     osmo-st-mgcp.star osmo-st-msc.star osmo-st-network.star osmo-st-sip.star \
     osmo-st-testphone.star petitparser.star petitparser-tests.star soapopera.star \
     osmo-st-openbsc-test.star iliad grease.star
	@echo "Built all"

iliad:
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/Swazoo/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/Core/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/More/HTML5Elements/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/More/Magritte/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/More/Formula/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/More/UI/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/More/Examples/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/More/RSS/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/More/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/More/Comet/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/package.xml
	$(GST_PACKAGE) $(GST_PACKAGE_ARGS) iliad-stable/Tests/package.xml

source-package:
	dpkg-buildpackage -S

"
 (C) 2010-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

TestCase subclass: GSM0808Test [
    <category: 'OsmoGSM-Tests'>
    testLAI [
        | lai res |
        res := #(16r72 16rF4 16r80) asByteArray.
        lai := LAI generateLAI: 274 mnc: 8.
        self assert: lai = res.

        lai := LAI parseFrom: res readStream.
        self assert: lai toMessage asByteArray = res.
    ]

    testCellIE [
        | ie res msg |

        res := #(5 8 0 114 244 128 32 18 117 48) asByteArray.
        msg := Osmo.MessageBuffer new.

        ie := GSMCellIdentifier initWith: 274 mnc: 8 lac: 8210 ci: 30000.
        ie writeOn: msg.

        self assert: msg asByteArray = res.

        ie := GSMCellIdentifier parseFrom: (res readStream skip: 1; yourself).
        self assert: ie mcc = 274.
        self assert: ie mnc = 8.
        self assert: ie lac = 8210.
        self assert: ie ci = 30000.
    ]

    testLayer3IE [
        | ie res msg |

        res := #(23 3 1 2 3) asByteArray.
        msg := Osmo.MessageBuffer new.
        ie := GSMLayer3Info initWith: #(1 2 3) asByteArray.
        ie writeOn: msg.

        self assert: msg asByteArray = res.

        ie := GSMLayer3Info parseFrom: (res readStream skip: 1; yourself).
        self assert: ie data = #(1 2 3) asByteArray.
    ]

    testComplL3 [
        | msg buf ie res |
        msg := IEMessage initWith: GSM0808Helper msgComplL3.

        msg addIe: (GSMCellIdentifier initWith: 274 mnc: 8 lac: 8210 ci: 30000).
        msg addIe: (GSMLayer3Info initWith: #(1 2 3) asByteArray).

        buf := Osmo.MessageBuffer new.
        msg writeOn: buf.

        res := #(16r57 16r05 16r08 16r00 16r72 16rF4 16r80 16r20 16r12
                 16r75 16r30 16r17 16r03 16r01 16r02 16r03) asByteArray.
        self assert: buf asByteArray = res
    ]

    testCauseIE [
        | buf ie res |
        res := #(4 1 32) asByteArray.

        ie := GSMCauseIE initWith: 32.
        buf := ie toMessage asByteArray.
        self assert: buf = res.

        ie := GSMCauseIE parseFrom: (res readStream skip: 1; yourself).
        self assert: ie cause = 32.
    ]

    testIEDecoding [
        | inp res |
        inp := #(16r57 16r05 16r08 16r00 16r72 16rF4 16r80 16r20 16r12
                 16r75 16r30 16r17 16r03 16r01 16r02 16r03) asByteArray.

        res := IEMessage decode: inp readStream with: GSM0808IE.
        self assert: res type = GSM0808Helper msgComplL3.
        self assert: res ies size = 2.
    ]

    testCIC [
        | res |
        res := (GSM0808CICIE new
                    multiplex: 1 timeslot: 20;
                    toMessage) asByteArray.
        self assert: res = #(1 0 52) asByteArray.

        res := (GSM0808CICIE initWithMultiplex: 1 timeslot: 20) toMessage asByteArray.
        self assert: res = #(1 0 52) asByteArray.
    ]

    testChanIE [
        | res |
        res := (GSM0808ChannelTypeIE buildPermittedSpeechList:
                                {GSM0808ChannelTypeIE speechFullRateVersion3.
                                  GSM0808ChannelTypeIE speechHalfRateVersion3}).
        self assert: res = #(161 37) asByteArray.

        res := ((GSM0808ChannelTypeIE
                initWith: (GSM0808ChannelTypeIE speechSpeech)
                audio: (GSM0808ChannelTypeIE chanSpeechHalfPrefNoChange))
                    audioCodecs: {GSM0808ChannelTypeIE speechFullRateVersion3.
                                  GSM0808ChannelTypeIE speechHalfRateVersion3};
                    yourself).
        self assert: res toMessage asByteArray = #(11 4 1 27 161 37) asByteArray.
    ]
]

TestCase subclass: BSSAPTest [
    <category: 'OsmoGSM-Tests'>

    testPrependManagment [
        | msg |
        msg := Osmo.MessageBuffer new.
        msg putByteArray: #(1 2 3) asByteArray.

        BSSAPHelper prependManagement: msg.
        self assert: msg asByteArray = #(0 3 1 2 3) asByteArray.
    ]

    testManagment [
        | man |

        man := BSSAPManagement initWith: #(1 2 3) asByteArray.
        self assert: man toMessage asByteArray = #(0 3 1 2 3) asByteArray.
    ]

    testParseManagement [
        | man |

        man := BSSAPMessage decode: #(0 3 1 2 3) asByteArray readStream.
        self assert: (man isKindOf: BSSAPManagement).
        self assert: man data = #(1 2 3) asByteArray.
    ]

    testPrependDTAP [
        | msg |
        msg := Osmo.MessageBuffer new.
        msg putByteArray: #(1 2 3) asByteArray.

        BSSAPHelper prependDTAP: msg dlci: 0.
        self assert: msg asByteArray = #(1 0 3 1 2 3) asByteArray.
    ]
]

TestCase subclass: GSM48Test [
    <category: 'OsmoGSM-Tests'>

    testFactoryClass [
        self
            assert: GSM48CCMessage isGSMBaseclass;
            deny: GSM48CCSetup isGSMBaseclass.
        self
            assert: GSM48MMMessage isGSMBaseclass;
            deny: GSM48LURequest isGSMBaseclass.
        self
            assert: GSM48SSMessage isGSMBaseclass;
            deny: GSM48SSReleaseComplete isGSMBaseclass.
        self
            assert: GSM48RRMessage isGSMBaseclass;
            deny: GSM48RRAssignmentComplete isGSMBaseclass.
    ]

    testKeySeqLu [
        | gsm msg res |
        res := #(16r70) asByteArray.
        msg := Osmo.MessageBuffer new.
        gsm := GSM48KeySeqLuType createDefault.
        gsm writeOnDirect: msg.

        self assert: msg asByteArray = res.

        self assert: (GSM48KeySeqLuType length: res readStream) = 1.
        gsm := GSM48KeySeqLuType parseFrom: res readStream.
        self assert: gsm val = 16r70.
    ]

    testLai [
        | gsm msg res |
        res := #(16r02 16rF2 16r50 16rFF 16rFE) asByteArray.
        msg := Osmo.MessageBuffer new.
        gsm := GSM48Lai createDefault.
        gsm mcc: 202; mnc: 5; lac: 65534.
        gsm writeOnDirect: msg.

        self assert: msg asByteArray = res.

        self assert: (GSM48Lai length: res readStream) = res size.
        gsm := GSM48Lai parseFrom: res readStream.
        self assert: gsm mcc = 202.
        self assert: gsm mnc = 5.
        self assert: gsm lac = 65534.
    ]

    testCM1 [
        | gsm msg res |
        res := #(16r33) asByteArray.
        msg := Osmo.MessageBuffer new.
        gsm := GSM48Classmark1 createDefault.
        gsm writeOnDirect: msg.

        self assert: msg asByteArray = res.

        self assert: (GSM48Classmark1 length: res readStream) = res size.
        gsm := GSM48Classmark1 parseFrom: res readStream.
        self assert: gsm cm1 = 16r33.
    ]

    testMI [
        | gsm msg res imsi |
        res := #(8 41 71 128 0 0 0 116 8) asByteArray.
        imsi := '274080000004780'.

        msg := Osmo.MessageBuffer new.
        gsm := GSM48MIdentity createDefault.
        gsm imsi: imsi.
        gsm writeOnDirect: msg.

        self assert: msg asByteArray = res.

        self assert: (GSM48MIdentity length: res readStream) = res size.
        gsm := GSM48MIdentity parseFrom: res readStream.
        self assert: gsm imsi = imsi.
    ]

    testMITMSI [
        | data res |
        data := #(23 16r05 16rF4 16r1E 16r35 16rC7 16r24) asByteArray.
        res := GSM48MIdentity parseFrom: (data readStream skip: 1).
        self assert: res toMessage asByteArray = data.
    ]

    testMITMSIGen [
        | res |
        res := (GSM48MIdentity new tmsi: #(1 2 3 4); toMessage) asByteArray.
        self assert: res = #(16r17 16r05 16rF4 1 2 3 4) asByteArray.
    ]

    testRejectCause [
        | rej msg target |
        target := #(11) asByteArray.

        msg := Osmo.MessageBuffer new.
        rej := GSM48RejectCause createDefault.
        rej writeOnDirect: msg.
        self assert: msg asByteArray = target.

        self assert: (GSM48RejectCause length: target readStream) = 1.
        rej := GSM48RejectCause parseFrom: target readStream.
        self assert: rej cause = 11.
    ]

    testLU [
        | gsm msg res |

        res := #(5 8 112 2 242 80 255 254 51 8 105 102 1 69 0 114 131 136) asByteArray.
        msg := Osmo.MessageBuffer new.
        gsm := GSM48LURequest new.
        (gsm lai) mcc: 202; mnc: 5; lac: 65534.
        (gsm mi) imsi: '666105400273888'.
        gsm writeOn: msg.

        self assert: msg asByteArray = res
    ]

    testAuRejParsing [
        | inp dec |

        inp := #(5 17 ) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec type = GSM48AuthRej messageType.
        self assert: GSM48AuthRej new toMessage asByteArray = inp.
    ]

    testImmediateAssignment [
        | inp dec |
        "Without 2D for the L2 pseudo length"
        inp := #(16r06 16r3F 16r03 16r20 16rE0 16r03 16r1A 16r0A 16r2F
                 16r00 16r00 16r2B 16r2B 16r2B 16r2B 16r2B 16r2B 16r2B
                 16r2B 16r2B 16r2B 16r2B) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec type = GSM48RRImmediateAssignCommand messageType.
        self assert: dec requestReference ra = 16r1A.
        self assert: dec requestReference t1 = 1.
        self assert: dec requestReference t2 = 15.
        self assert: dec requestReference t3 = 17.
        self assert: dec toMessage asByteArray = inp.
    ]

    testRRChannelRelease [
        | inp dec |
        inp := #(16r06 16r0D 16r00) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec type = GSM48RRChannelRelease messageType.
        self assert: dec toMessage asByteArray = inp.
    ]

    testCalledBCDNumber [
        | dec |
        dec := GSMCalledBCDNumber initWithData: #(145 51 83 102 246) asByteArray.
        self assert: dec numberType = GSMCalledBCDNumber typeInternational.
        self assert: dec numberPlan = GSMCalledBCDNumber planISDN.
        self assert: dec number = '3335666'.

        dec := GSMCalledBCDNumber createDefault.
        self assert: dec data = #(0) asByteArray.
        dec encode: GSMCalledBCDNumber typeInternational
            plan: GSMCalledBCDNumber planISDN nr: '3335666'.
        self assert: dec data = #(145 51 83 102 246) asByteArray.
    ]

    testCCStatus [
        | dec inp |
        inp := #(16r03 16r3D 16r03 16rE0 16rE1 16r42 16rC1) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec toMessage asByteArray = inp.
    ]

    testGSMBearerParser [
        | cap decoded |

        cap := GSMBearerCap parseFrom: #(16r04 16r60 16r02 16r00 16r81) readStream.
        decoded := GSMBearerCapFromMS parse: cap data readStream.

        self deny: decoded octet3 isNil.
        self assert: decoded octet3a size = 3.

        self assert: decoded octet3 informationTransferCapability = GSMBearerCapOctet3 transferCapSpeech.
        self assert: decoded octet3 transferMode = GSMBearerCapOctet3 transferModeCircuit.
        self assert: decoded octet3 codingStandard = GSMBearerCapOctet3 radioCodStdGSM.
        self assert: decoded octet3 radioChannelRequirement = GSMBearerCapOctet3 radioChReqDualFullPref.


        self assert: decoded octet3a first speechVersionIndication = GSMBearerCapOctet3a speechFullRateVersion2.
        self assert: decoded octet3a first spare = 0.
        self assert: decoded octet3a first ctm = GSMBearerCapOctet3a ctmTextTelephonyNotSupported.
        self assert: decoded octet3a first coding = GSMBearerCapOctet3a codingForTransferCapability.

        self assert: decoded octet3a second speechVersionIndication = GSMBearerCapOctet3a speechFullRateVersion1.
        self assert: decoded octet3a second spare = 0.
        self assert: decoded octet3a second ctm = GSMBearerCapOctet3a ctmTextTelephonyNotSupported.
        self assert: decoded octet3a second coding = GSMBearerCapOctet3a codingForTransferCapability.

        self assert: decoded octet3a third speechVersionIndication = GSMBearerCapOctet3a speechHalfRateVersion1.
        self assert: decoded octet3a third spare = 0.
        self assert: decoded octet3a third ctm = GSMBearerCapOctet3a ctmTextTelephonyNotSupported.
        self assert: decoded octet3a third coding = GSMBearerCapOctet3a codingForTransferCapability.
    ]

    testGSMBearerCreate [
        | octet3 |

        octet3 := GSMBearerCapOctet3 fromByte: 0.
        octet3 informationTransferCapability: octet3 class transferCapOtherITC.
        self assert: octet3 data = 2r00000101.
        octet3 transferMode: 1.
        self assert: octet3 data = 2r00001101.
        octet3 codingStandard: 1.
        self assert: octet3 data = 2r00011101.
        octet3 radioChannelRequirement: 2r10.
        self assert: octet3 data = 2r01011101.
    ]

    testClassmarkChange [
        | dec inp |
        inp := #(16r06 16r16 16r03 16r33 16r19 16rA2) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec type = GSM48RRClassmarkChange messageType.
        self assert: dec toMessage asByteArray = inp.
    ]

    testTMSI [
        "smoke test"
        GSM48TMSIReallocationCommand new toMessage.
        GSM48TMSIReallocationComplete new toMessage
    ]

    testMMInformation [
        | dec inp |
        inp := #(16r05 16r32 16r43 16r08 16r87 16r4F 16r78 16rD9 16r2D 16r9C 16r0E 16r01 16r45 16r08 16r87 16r4F 16r78 16rD9 16r2D 16r9C 16r0E 16r01 16r47 16r21 16r01 16r62 16r91 16r14 16r35 16r80) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec toMessage asByteArray = inp.
    ]

    testChannelModify [
	| dec inp |
	inp := #(16r06 16r10 16r0A 16rE3 16r68 16r21) asByteArray.
	dec := GSM48MSG decode: inp readStream.
	self assert: dec toMessage asByteArray = inp.

	dec channelMode mode: GSM48ChannelMode modeSpeechVersion2.
	self assert: dec toMessage asByteArray = inp.
    ]

    testChannelModifyAck [
	| dec inp |
	inp := #(16r06 16r17 16r0A 16rE3 16r68 16r21) asByteArray.
	dec := GSM48MSG decode: inp readStream.
	self assert: dec toMessage asByteArray = inp.
    ]

    testHandoverCommand [
        | dec inp |
        inp := #(16r06 16r2B 16rFF 16r29 16r0B 16rE3 16r29 16r00 16r00) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec toMessage asByteArray = inp.

        "Now test some decoding inside"
        self assert: dec cellDescription bcc = 7.
        self assert: dec cellDescription ncc = 7.
        self assert: dec cellDescription bcch = 809.
        self assert: dec handoverReference value = 0.
        self assert: dec channelDescription2 channelType = 1.
        self assert: dec channelDescription2 timeSlot = 3.
        self assert: dec channelDescription2 arfcn = 809.
    ]

    testCipheringModeCommand [
        | dec inp |
        inp := #(16r06 16r35 16r11) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec toMessage asByteArray = inp.
    ]

    testCipheringModeComplete [
        | dec inp |
        inp := #(16r06 16r32) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec toMessage asByteArray = inp.
    ]

    testChannelModeModify [
        | dec inp |
        inp := #(16r06 16r10 16r12 16rE3 16r69 16r41 16r03 16r02 16r28 16r04) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec toMessage asByteArray = inp.
    ]

    testMMInformation [
        | dec inp |
        inp := #(16r05 16r32 16r45 16r08 16r87 16r4F 16rF7 16r35
                 16r6C 16r2F 16rCF 16r01 16r47 16r31 16r80 16r82
                 16r90 16r80 16r13 16r80 16r49 16r01 16r01) asByteArray.
        dec := GSM48MSG decode: inp readStream.
        self assert: dec toMessage asByteArray = inp.
    ]

    testCCProgress [
        | dec inp |

        inp := #[16r83 16r03 16r02 16rEA 16r81].
        dec := GSM48MSG decode: inp readStream.
        self assert: dec toMessage asByteArray = inp.
    ]
]

SCCPHandler subclass: TestSCCPHandler [
    <category: 'OsmoGSM-Tests'>
    assignSrcRef [
        ^ 666
    ]
]

TestCase subclass: TestMessages [
    <category: 'OsmoGSM-Tests'>

    testMsgParser [
        | msg bssap bssmap ies l3 gsm48 inp |

        inp := #(1 154 2 0 2 2 4 2 66 254 15 32 0 30 87
                 5 8 0 114 244 128 16 3 156 64 23 17 5 8
                 112 0 240 0 0 0 51 7 97 102 102 102 102
                 102 246 0 ) asByteArray.
        msg := MSGParser parse: inp.
        self assert: (msg isKindOf: Osmo.SCCPConnectionRequest).

        bssap := msg data.
        self assert: (bssap isKindOf: BSSAPManagement).

        bssmap := bssap data.
        self assert: (bssmap isKindOf: IEMessage).

        ies := bssmap ies.
        self assert: ies size = 2.

        l3 := bssmap findIE: (GSMLayer3Info elementId) ifAbsent: [
            self assert: false.
        ].

        self assert: (l3 isKindOf: GSMLayer3Info).

        gsm48 := l3 data.
        self assert: (gsm48 isKindOf: GSM48LURequest).

        self assert: gsm48 mi imsi = '666666666666'.

        self assert: msg toMessage asByteArray = inp.
    ]

    testMsgParserDt1 [
        | inp msg bssap gsm48 |
        inp := #(6 154 2 0 0 1 6 1 0 3 5 4 11 ) asByteArray.

        msg := MSGParser parse: inp.
        self assert: (msg isKindOf: Osmo.SCCPConnectionData).

        bssap := msg data.
        self assert: (bssap isKindOf: BSSAPDTAP).

        gsm48 := bssap data.
        self assert: (gsm48 isKindOf: GSM48LUReject).

        self assert: msg toMessage asByteArray = inp.
    ]

    testMsgparserDt1Clear [
        | inp msg bssap bssmap |
        inp := #(6 154 2 0 0 1 6 0 4 32 4 1 32) asByteArray.
        msg := MSGParser parse: inp.
        self assert: (msg isKindOf: Osmo.SCCPConnectionData).

        bssap := msg data.
        self assert: (bssap isKindOf: BSSAPManagement).

        bssmap := bssap data.
        self assert: (bssmap isKindOf: IEMessage).

        self assert: msg toMessage asByteArray = inp.
    ]

    testRandomMessages [
        | inp msg |
        "This only tests some parsing... it does not verify the content"

        inp := #(6 1 8 101 0 1 3 0 1 33 ) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        inp := #(6 48 4 5 0 1 22 1 0 19 5 18 1 83 3 123 16 155 119 176 138 215 28 107 26 47 193 59 248 ) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        inp := #(6 1 8 104 0 1 9 1 0 6 5 84 253 230 198 47) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        inp := #(6 46 4 5 0 1 20 1 0 17 5 2 114 244 128 16 3 23 8 41 34 1 96 16 85 37 115) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.


        "Identity Request"
        inp := #(16r06 16r3A 16r04 16r05 16r00 16r01 16r06 16r01 16r00 16r03 16r05 16r18 16r01) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Identity Response"
        inp := #(16r06 16r01 16r08 16r76 16r00 16r01 16r0E 16r01 16r00 16r0B 16r05 16r59 16r08 16r29 16r20 16r10 16r31 16r61 16r35 16r45 16r06) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "CM Service Request"
        inp := #(1 205 4 5 2 2 4 2 66 254 15 33 0 31 87 5 8 0 114 244 128 16 3 156 64 23 16 5 36 17 3 51 25 129 8 41 32 1 153 118 6 1 152 33 1 0) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "IMSI Detach Ind"
        inp := #(1 255 4 5 2 2 4 2 66 254 15 29 0 27 87 5 8 0 114 244 128 16 3 156 64 23 12 5 1 51 8 41 65 112 6 16 9 71 34 33 1 0 ) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "BSSMAP paging"
        inp := #(16r09 16r00 16r03 16r07 16r0B 16r04 16r43 16r07 16r00 16rFE 16r04 16r43 16r5C 16r00 16rFE 16r12 16r00 16r10 16r52 16r08 16r08 16r29 16r22 16r88 16r81 16r04 16r56 16r44 16r24 16r1A 16r03 16r05 16r10 16r03) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Ciphermode Command"
        inp := #(6 0 0 72 0 1 14 0 12 83 10 9 3 8 90 152 155 24 30 20 226 ) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Ciphermode Complete"
        inp := #(16r06 16r01 16r03 16r23 16r00 16r01 16r05 16r00 16r03 16r55 16r2C 16r02) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

	"Ciphering mode complete..."
        inp := #[16r06 16r01 16r01 16r5B 16r00 16r01 16r14 16r00 16r12 16r55 16r20 16r0D 16r06 16r32 16r17 16r09 16r33 16r15 16r49 16r00 16r93 16r89 16r31 16r62 16rF4 16r2C 16r02].
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.


        "Assignment Command"
        inp := #(6 0 0 72 0 1 11 0 9 1 11 3 1 10 17 1 0 20 ) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Assignment Complete"
        inp := #(6 1 3 35 0 1 11 0 9 2 21 0 33 152 44 2 64 17) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Classmark update"
        inp := #(6 1 3 35 0 1 16 0 14 84 18 3 51 25 145 19 6 96 20 69 0 1 0) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "MO Setup message"
        inp := #(6 1 3 35 0 1 21 1 128 18 3 69 4 6 96 4 2 0 5 129 94 6 145 83 132 54 23 121 ) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "MO Proceeding"
        inp := #(6 0 0 72 0 1 5 1 0 2 131 2) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Alerting"
        inp := #(6 0 0 72 0 1 9 1 0 6 131 1 30 2 234 129) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Connect"
        inp := #(6 0 0 72 0 1 5 1 0 2 131 7) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Connct acknowledge"
        inp := #(6 1 3 35 0 1 5 1 128 2 3 15) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Disconnect"
        inp := #(6 0 0 72 0 1 8 1 0 5 131 37 2 225 144) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Release"
        inp := #(6 1 3 35 0 1 5 1 128 2 3 109) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Release Complete"
        inp := #(6 0 0 72 0 1 9 1 0 6 131 42 8 2 225 144) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "CM Service Reject"
        inp := #(6 0 103 68 0 1 6 1 0 3 5 34 11) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

        "Supplementary services here. GSM48SSRegister"
        inp := #( 16r06 16r01 16r0C 16rAA 16r00 16r01 16r1F 16r01 16r80 16r1C 16r0B 16r7B 16r1C 16r15 16rA1 16r13 16r02 16r01 16r04 16r02 16r01 16r3B 16r30 16r0B 16r04 16r01 16r0F 16r04 16r06 16r2A 16rD5 16r4C 16r16 16r1B 16r01 16r7F 16r01 16r00) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.

	"Release Complete"
        inp := #(16r06 16r0B 16rEA 16r55 16r00 16r01 16r4E 16r01 16r00 16r4B 16r8B 16r2A 16r08 16r02 16rE1 16r90 16r1C 16r43 16rA2 16r41 16r02 16r01 16r04 16r30 16r3C 16r02 16r01 16r3B 16r30 16r37 16r04 16r01 16r0F 16r04 16r32 16rD9 16r77 16r5D 16r0E 16r92 16r97 16rDB 16rE1 16rB4 16r3B 16rED 16r3E 16r83 16rC4 16r61 16r76 16rD8 16r3D 16r2E 16r83 16rD2 16r73 16r5D 16rEC 16r06 16rA3 16r81 16rDA 16r69 16r37 16rAB 16r8C 16r87 16rA7 16rE5 16r69 16rF7 16r19 16rF4 16r76 16rEB 16r62 16rB0 16r16 16rEC 16rD6 16r92 16rC1 16r62 16r30) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.
    ]

    testCCSetupWithCC [
        | inp msg |
        inp := #(16r01 16r0A 16r4B 16r04 16r02 16r02 16r04 16r02 16r42 16rFE 16r0F 16r23 16r00 16r21 16r57 16r05 16r08 16r00 16r72 16rF4 16r80 16r10 16r1C 16r9C 16r41 16r17 16r14 16r03 16r45 16r04 16r06 16r60 16r04 16r02 16r00 16r05 16r81 16r5E 16r04 16r80 16r4A 16r55 16rF1 16r15 16r02 16r11 16r01 16r00) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.
    ]

    testCCSetup3G [
        | inp msg |

        inp := #(16r01 16r91 16r0D 16r03 16r02
                 16r02 16r04 16r02 16r42 16rFE
                 16r0F 16r2A 16r00 16r28 16r57
                 16r05 16r08 16r00 16r72 16rF4
                 16r80 16r10 16r1C 16r9C 16r41
                 16r17 16r1B
                 16r03 16r45 16r04 16r06 16r60
                 16r04 16r05 16r02 16r00 16r81
                 16r5E 16r04 16r81 16r4A 16r55
                 16rF6 16rA1 16r40 16r08 16r04
                 16r02 16r60 16r00 16r00 16r02
                 16r1F 16r00 16r00) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp.
    ]

    testRRComplete [
        | inp msg |
        inp := #(16r06 16rDA 16r28 16rCE 16r00 16r01 16r06 16r01 16r00 16r03 16r06 16r29 16r00) asByteArray.
        msg := MSGParser parse: inp.
        self assert: msg toMessage asByteArray = inp
    ]
]

Object subclass: DummyConnection [
    <category: 'OsmoGSM-Tests'>

    send: aMsg with: aProto [
        "NOP"
    ]
]

Object subclass: DirectConnection [
    | handler |
    <category: 'OsmoGSM-Tests'>

    DirectConnection class >> initWith: aHandler [
        ^ self new
            instVarNamed: #handler put: aHandler; yourself.
    ]

    send: aMsg with: aProto [
        handler handleMsg: aMsg.
    ]
]

SCCPConnection subclass: SCCPMockConnection [
    | queue |
    <category: 'OsmoGSM-Tests'>

    readQueue [ ^ queue ifNil: [queue := SharedQueue new]]

    next [
        | res |

        res := queue next.
        res = 0 ifTrue: [
            ^ SystemExceptions.EndOfStream signal
        ].

        ^ res
    ]

    data: aDT [
        self readQueue nextPut: aDT data
    ]

    terminate [
        self readQueue nextPut: 0
    ]
]

SCCPHandler subclass: SCCPHandlerNonRec [
    <category: 'OsmoGSM-Tests'>

    SCCPHandlerNonRec class >> dissectMSG: aMsg [
        ^ Osmo.SCCPMessage decode: aMsg asByteArray.
    ]

    connectionSpecies [
        ^ SCCPMockConnection
    ]
]

TestCase subclass: SCCPHandlerTest [
    <category: 'OsmoGSM-Tests'>
    <comment: 'Test that one can do easy connection handling'>

    testDisconnectWithMSG [
        | handler con amount result |
        "This should test that we process all pending data in a socket"

        handler := SCCPHandlerNonRec new.
        handler connection: DummyConnection new.

        con := SCCPMockConnection on: handler.
        con connectionRequest: '123' asByteArray.
        handler handleMsg: (Osmo.SCCPConnectionConfirm initWithSrc: 567 dst: con srcRef) toMessage asByteArray.
        handler handleMsg: (Osmo.SCCPConnectionData initWith: con srcRef data: '123' asByteArray) toMessage asByteArray.
        handler handleMsg: (Osmo.SCCPConnectionReleased initWithDst: con srcRef src: 567 cause: 16rFE) toMessage asByteArray.


        self assert: con readQueue isEmpty not.
        self shouldnt: [con next] raise: SystemExceptions.EndOfStream description: 'Should read the 123'.

        self assert: con readQueue isEmpty not.
        self should: [con next] raise: SystemExceptions.EndOfStream description: 'Now we should be at the end'.

        self assert: con readQueue isEmpty.
        handler handleMsg: (Osmo.SCCPConnectionData initWith: con srcRef data: '123' asByteArray) toMessage asByteArray.
        self assert: con readQueue isEmpty.
    ]

    testConnectionHandling [
        | client server con serverCon|
        "Test opening and closing connections"

        client := SCCPHandlerNonRec new.
        server := SCCPHandlerNonRec new.

        "Connect both handlers directly"
        client connection: (DirectConnection initWith: server).
        server connection: (DirectConnection initWith: client).

        "Establish the connection"
        con := SCCPMockConnection on: client.
        con connectionRequest: '123' asByteArray.
        self assert: con state = SCCPConnection stateConnected.

        self assert: server connections size = 1.
        serverCon := server connections first.
        self assert: serverCon state = SCCPConnection stateConnected.

        "Check if we were able to read data"
        self assert: serverCon readQueue isEmpty not.
        self assert: serverCon next = '123' asByteArray.

        "Now close the connection"
        serverCon release.
        self assert: serverCon state = SCCPConnection stateReleaseComplete.
        self assert: con state = SCCPConnection stateReleaseComplete.

        self assert: client connections size = 0.
        self assert: server connections size = 0.

        "Verify we will now get exceptions"
        self should: [con next] raise: SystemExceptions.EndOfStream.
        self should: [serverCon next] raise: SystemExceptions.EndOfStream.
    ]
]

TestCase subclass: GSMEncodingTest [
    <category: 'OsmoGSM-Tests'>
    <comment: 'I test the classic 7bit GSM encoding'>

    test7BitEncode [
        | wanted res |

        wanted := #(16rD9 16r77 16r5D 16r0E 16r92 16r97 16rDB 16rE1 16rB4 16r3B 16rED 16r3E 16r83 16rC4 16r61 16r76 16rD8 16r3D 16r2E 16r83 16rD2 16r73 16r5D 16rEC 16r06 16rA3 16r81 16rDA 16r69 16r37 16rAB 16r8C 16r87 16rA7 16rE5 16r69 16rF7 16r19 16rF4 16r76 16rEB 16r62 16rB0 16r16 16rEC 16rD6 16r92 16rC1 16r62 16r30) asByteArray.

        res := 'Your remaining balance is:1704 min,expiring on:10-07-2010' asGSM7Bit.
        self assert: res = wanted
    ]

    test7BitDecode [
        | wanted res |

        wanted := 'Your remaining balance is:1704 min,expiring on:10-07-2010'.
        res := #[16rD9 16r77 16r5D 16r0E 16r92 16r97 16rDB 16rE1 16rB4 16r3B 16rED 16r3E 16r83 16rC4 16r61 16r76 16rD8 16r3D 16r2E 16r83 16rD2 16r73 16r5D 16rEC 16r06 16rA3 16r81 16rDA 16r69 16r37 16rAB 16r8C 16r87 16rA7 16rE5 16r69 16rF7 16r19 16rF4 16r76 16rEB 16r62 16rB0 16r16 16rEC 16rD6 16r92 16rC1 16r62 16r30] decodeGSM7Bit.

        self assert: res equals: wanted.
    ]

    testExpand [
        | wanted res |
        wanted := 'Your remaining balance is:1704 min,expiring on:10-07-2010' asByteArray.
        res := GSMDecoding expand: #[217 119 93 14 146 151 219 225 180 59 237 62 131 196 97 118 216 61 46 131 210 115 93 236 6 163 129 218 105 55 171 140 135 167 229 105 247 25 244 118 235 98 176 22 236 214 146 193 98 48].
        self assert: res equals: wanted
    ]

    testUSSDEncode [
        | wanted res |

        wanted := #(49 217 140 86 179 221 112 57 88 76 54 163 213 26) asByteArray.
        self assert: '123456789012345' asUSSD7Bit = wanted.
    ]

    testUSSDDecode [
        | wanted |

        wanted := '123456789012345'.
        self assert: #(49 217 140 86 179 221 112 57 88 76 54 163 213 26) asByteArray decodeUSSD7Bit = wanted.
    ]

    testUSSDEncodeCR [
        | wanted res |

        wanted := #(49 217 140 86 179 221 26 13) asByteArray.
        self assert: ('1234567', Character cr asString) asUSSD7Bit = wanted.
    ]

    testUSSDDecodeCR [
        | wanted |
        "This tests that the code as adding an extra CR and that it was
         not removed from the input."
        wanted := '1234567', Character cr asString, Character cr asString.
        self assert: #(49 217 140 86 179 221 26 13) asByteArray decodeUSSD7Bit = wanted.
    ]
]

TestCase subclass: BitfieldTest [
    <category: 'OsmoGSM-Tests'>
    <comment: 'I test the Bitfield class'>

    testFromByte [
        | res |

        res := GSMBitField fromByte: 16r5A.
        self assert: (res bitsFrom: (1 to: 1)) = 2r0.
        self assert: (res bitsFrom: (2 to: 2)) = 2r1.
        self assert: (res bitsFrom: (1 to: 2)) = 2r10.
        self assert: (res bitsFrom: (3 to: 4)) = 2r10.
        self assert: (res bitsFrom: (5 to: 8)) = 2r0101.
        self assert: (res bitsFrom: (5 to: 5)) = 2r1.
    ]

    testByteSet [
        | res |

        res := GSMBitField fromByte: 16rFF.
        res atBits: (1 to: 2) put: 2r10.
        res atBits: (3 to: 4) put: 2r10.
        res atBits: (5 to: 8) put: 2r0101.

        self assert: res data = 16r5A.
    ]
]

TestCase subclass: TestGSM48ProgressIE [
    <category: 'OsmoGSM-Tests'>
    testDefault [
        | prog |
        prog := GSMProgress createDefault.
        self
            assert: prog isGSMToPLMNS;
            assert: prog coding = GSMProgress codingStandardGSMToPLMNS;
            assert: prog location = GSMProgress locationNetBeyondInterworkingPoint;
            assert: prog progress = GSMProgress progressCallNotEndToEnd.
    ]

    testManipulate [
        | prog |
        prog := GSMProgress createDefault
            coding: GSMProgress codingStandardNational;
            location: GSMProgress locationPrivateNetRemoteUser;
            progress: GSMProgress progressOrigNotInISDN;
            yourself.

        prog data printNl.
"
        self deny: prog isGSMToPLMNS.
        prog data printNl.
        self assert: prog coding = GSMProgress codingStandardNational.
        self assert: prog location = GSMProgress locationPrivateNetRemoteUser.
"
        self assert: prog progress = GSMProgress progressOrigNotInISDN.
    ]
]

TestCase subclass: GSMNumberDigitsTest [
    <category: 'OsmoGSM-Test'>

    testDecodeFrom [
        | number res |
        number := #(73 132 50 23 120).
        res := GSMNumberDigits decodeFrom: number.
        self assert: res = '9448237187'.

        number := #(73 132 50 23 120 186 220 174).
        res := GSMNumberDigits decodeFrom: number.
        self assert: res = '9448237187*#abc*'.

        number := #(114 4 8 0 0 48 0 240).
        res := GSMNumberDigits decodeFrom: number.
        self assert: res = '274080000003000'.
    ]

    testEncodeFrom [
        | res |
        res := GSMNumberDigits encodeFrom: '9448237187*#abc*'.
        self assert: res = #(73 132 50 23 120 186 220 174) asByteArray.

        res := GSMNumberDigits encodeFrom: '274080000003000'.
        self assert: res = #(114 4 8 0 0 48 0 240) asByteArray.
    ]

]

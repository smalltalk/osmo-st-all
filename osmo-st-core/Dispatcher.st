"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: Dispatcher [
    | queue dispatch quit |
    <category: 'OsmoCore-Core'>
    <comment: 'I run tasks from the same context.'>

    Dispatcher class >> instance [
        <category: 'singleton'>
        ^ Smalltalk at: #OsmoDispatcher ifAbsentPut: [Dispatcher new].
    ]

    Dispatcher class >> new [
        <category: 'private'>
        ^ super new
            initialize;
            addToBeFinalized;
            yourself
    ]

    initialize [
        <category: 'private'>
        quit := false.
        queue := SharedQueue new.
        dispatch := [
            Processor activeProcess name: 'OsmoDispatcher'.
	    [quit]
              whileFalse: [
                self dispatch]
          ] forkAt: Processor highIOPriority.
    ]

    dispatchBlock: aBlock [
        <category: 'insert'>
        queue nextPut: aBlock.
    ]

    dispatch [
        | block sem |
        block := queue next.
        sem := Semaphore new.

        "Run the code in a new process as the debugger might terminate this
        and then the dispatcher would not dispatch anymore. Use a Semaphore
        to make sure we keep on processing items in order."
        [[
        block on: Error do: [:error |
            error logException: ('dispatch failed on "', block printString, '".') area: #core.
        ]] ensure: [sem signal]] fork.

        sem wait.
    ]   
]

Eval [
    Dispatcher instance.
]

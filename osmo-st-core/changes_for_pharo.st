TimerScheduler extend [
    TimerScheduler class >> initialize [
        <category: 'loading'>
        "Pharo requires us to do some post-processing"
        Smalltalk addToStartUpList: self.
        Smalltalk addToShutDownList: self.
        ^self instance.
    ]

    TimerScheduler class >> startUp [
        Smalltalk at: #OsmoTimeScheduler ifPresent: [:timer | timer doStartUp].
    ]

    TimerScheduler class >> shutDown: quitting [
        Smalltalk at: #OsmoTimeScheduler ifPresent: [:timer | timer doShutDown].
    ]

    doShutDown [
        <category: 'PharoHacks'>
        loop ifNil: [^self].
        quit := true.
        processExited wait.
        Transcript nextPutAll: 'Stopped the TimerScheduler process'; cr.
    ]

    doStartUp [
        <category: 'PharoHacks'>
        loop ifNotNil: [^self error: 'The loop should have vanished'].
        Transcript nextPutAll: 'Starting the TimerScheduler loop again'; cr.
        quit := false.
        self startLoop.
    ]
]

Dispatcher class extend [
    initialize [
        ^ self instance
    ]
]

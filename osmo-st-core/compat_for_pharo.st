"This is to be easily loadable on Pharo 1.4 and up"

Object extend [
    addToBeFinalized [
        <category: '*OsmoCore'>
        "No idea how to implement it"
    ]
]

Semaphore extend [
    signals [
        <category: '*OsmoCore'>
        "Used in our testcase"
        ^ excessSignals
    ]
]

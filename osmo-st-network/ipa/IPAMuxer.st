"
 (C) 2010-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: IPADemuxer [
    | socket |

    <category: 'OsmoNetwork-IPA'>
    <comment: 'I know how to demultiplex data from a socket. Give
    me the socket and I read from it and provide you a tuple.'>

    IPADemuxer class >> initOn: aSocket [
        <category: 'creation'>
	^ (self new)
	    socket: aSocket;
	    yourself.
    ]

    next [
	"Return a tuple of stream and bytearray"
        <category: 'reading'>

	| size stream data |

	size := socket nextUshort swap16.
	stream := socket nextByte.
	data := socket next: size.

        "I know about extensions. Check if this is..."
        stream = IPAConstants protocolOSMO ifTrue: [
            stream := Array with: stream with: data first asInteger.
            data := data allButFirst.
        ].

	^ Array with: stream with: data.
    ]

    socket: aSocket [
        <category: 'accessing'>
	socket := aSocket.
    ]
]

Object subclass: IPAMuxer [
    | socket |

    <category: 'OsmoNetwork-IPA'>
    <comment: 'I can multiplex data according to the IPA protocol. You
    will need to give me a Socket or a SharedQueue and I will mux the
    data you provide me with.'>

    IPAMuxer class >> initOn: aSocket [
        <category: 'creation'>
	^ (self new)
	    socket: aSocket;
	    yourself.
    ]

    prepareNext: aData with: aStream [
	"Write the data onto the stream"
	| msg |
        <category: 'accessing'>

	aData size > 65535
	    ifTrue: [
                self logError: 'Too much data' area: #ipa.
	        self error: 'Too much data'.
	].

	msg := MessageBuffer new.

        aStream isArray
            ifTrue:  [
                msg putLen16: aData size + aStream size - 1.
                msg putByteArray: aStream asByteArray]
            ifFalse: [
                msg putLen16: aData size.
                msg putByte: aStream].

	msg putByteArray: aData.

	^ msg asByteArray.
    ]

    nextPut: aData with: aStream [
        <category: 'encoding'>
	socket nextPut: (self prepareNext: aData with: aStream).
    ]

    socket: aSocket [
        <category: 'accessing'>
	socket := aSocket.
    ]
]

Eval [
    | msg dt socket dgram |

    PackageLoader fileInPackage: #Sockets.
    PackageLoader fileInPackage: #OsmoNetwork.
    msg := Osmo.M2UAMSG fromClass: Osmo.M2UAConstants clsMAUP type: Osmo.M2UAConstants maupData.
    msg addTag: (Osmo.M2UATag initWith: Osmo.M2UAConstants tagIdentText data: 'm2ua' asByteArray).
    msg addTag: (Osmo.M2UATag initWith: Osmo.M2UAConstants tagData data: #(0 0 0 0 0 0 0 0 0 0) asByteArray).
    dt := msg toMessage asByteArray.

    dt inspect.

    socket := Sockets.DatagramSocket new.
    dgram := Sockets.Datagram data: dt.
    dgram port: 5001.
    dgram address: Sockets.SocketAddress loopbackHost.

    socket nextPut: dgram.
]

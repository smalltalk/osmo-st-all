Message type 2.1 F 1
Backward call indicators 3.5 F 2
Optional backward call indicators 3.37 O 3
Backward GVNS 3.62 O 3-?
Connected number (Note 2) 3.16 O 4-?
Call reference (national use) 3.8 O 7
User-to-user indicators 3.60 O 3
User-to-user information 3.61 O 3-131
Access transport 3.3 O 3-?
Network specific facility (national use) 3.36 O 4-?
Generic notification indicator (Note 1) 3.25 O 3
Remote operations (national use) 3.48 O 8-?
Transmission medium used 3.56 O 3
Echo control information 3.19 O 3
Access delivery information 3.2 O 3
Call history information 3.7 O 4
Parameter compatibility information 3.41 O 4-?
Service activation 3.49 O 3-?
Generic number (Notes 1 and 2) 3.26 O 5-?
Redirection number restriction 3.47 O 3
Conference treatment indicators 3.76 O 3-?
Application transport parameter (Note 3) 3.82 O 5-?
HTR information 3.89 O 4-?
Pivot routing backward information 3.95 O 3-?
Redirect status (national use) 3.98 O 3
End of optional parameters 3.20 O 1

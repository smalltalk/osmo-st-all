"
 (C) 2010-2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"
"Test Case for Osmo-Network"

TestCase subclass: SCCPTests [
    <category: 'OsmoNetwork-Tests'>

    SCCPTests class >> packageNamesUnderTest [
        <category: 'accessing'>
        ^ #('OsmoNetwork')
    ]

    testCreateForSSN [
        | addr |
        addr := SCCPAddress createForSSN: 'hlr'.
        self assert: addr subSystemNumber = SCCPAddress ssnHLR.
        self assert: addr routedOnSSN.
        self assert: addr globalTitle isNil.
        self assert: addr pointCode isNil.
    ]

    testPNCCreate [
        | pnc |

        pnc := SCCPPNC new.
        pnc at: SCCPHelper pncData put: #(1 2 3) asByteArray.
        self assert: pnc toMessage asByteArray = #(15 3 1 2 3 0) asByteArray.
    ]

    testReleasedFormat [
        | rlsd msg |
        rlsd := SCCPConnectionReleased initWithDst: 16r401 src: 16r1F0A01 cause: 16rFE.
        msg := rlsd toMessage asByteArray.

        self assert: msg = #(4 1 4 0 1 16r0A 16r1F 16rFE 1 0) asByteArray
    ]

    testDT1 [
        | dt1 msg target |
        target := #(6 1 4 0 0 1 4 49 50 51 52) asByteArray.
        dt1 := SCCPConnectionData initWith: 16r401 data: '1234' asByteArray.
        msg := dt1 toMessage asByteArray.

        self assert: msg = target.

        dt1 := SCCPMessage decode: target.
        self assert: dt1 dst = 16r401.
        self assert: dt1 data = '1234' asByteArray.
        self assert: dt1 toMessage asByteArray = target.
    ]

    testCR [
        | cr msg target |
        target := #(1 191 0 3 2 2 4 2 66 254 15 4 49 50 51 52 0) asByteArray.

        "encode it"
        cr := SCCPConnectionRequest
                    initWith: 16r0300BF
                    dest: (SCCPAddress createWith: 254)
                    data: '1234' asByteArray.
        msg := cr toMessage asByteArray.

        self assert: msg = target.

        "now decode it"
        cr := SCCPMessage decode: target.
        self assert: (cr isKindOf: SCCPConnectionRequest).
        self assert: cr src = 16r0300BF.
        self assert: cr dest asByteArray = (SCCPAddress createWith: 254) asByteArray.
        self assert: cr data = '1234' asByteArray.

        "now encode it again"
        self assert: cr toMessage asByteArray = target.
    ]

    testCC [
        | target cc |

        target := #(2 191 0 3 1 3 176 2 1 0) asByteArray.

        cc := SCCPConnectionConfirm
                    initWithSrc: 16rB00301 dst: 16r0300BF.
        self assert: cc toMessage asByteArray = target.

        cc := SCCPMessage decode: target.
        self assert: (cc isKindOf: SCCPConnectionConfirm).
        self assert: cc dst = 16r0300BF.
        self assert: cc src = 16rB00301.

        self assert: cc toMessage asByteArray = target.
    ]

    testRlsd [
        | target rlsd |

        target := #(4 154 2 0 66 5 5 0 1 0 ) asByteArray.
        rlsd := SCCPConnectionReleased
                    initWithDst: 16r0029A src: 16r50542
                    cause: 0.
        self assert: rlsd toMessage asByteArray = target.

        rlsd := SCCPMessage decode: target.
        self assert: rlsd dst = 16r0029A.
        self assert: rlsd src = 16r50542.
        self assert: rlsd cause = 0.
        self assert: rlsd toMessage asByteArray = target.
    ]

    testCreateRLSD [
        | target rlsd |
        target := #(4 154 2 0 66 5 5 0 1 0 ) asByteArray.
        rlsd := SCCPHelper createRLSD: 16r50542 dest: 16r0029A cause: 0.
        self assert: rlsd asByteArray = target.
    ]

    testRlc [
        | target rlc |

        target := #(5 1 8 119 62 4 5 ) asByteArray.
        rlc := SCCPConnectionReleaseComplete
                    initWithDst: 16r770801 src: 16r05043E.
        self assert: rlc toMessage asByteArray = target.

        rlc := SCCPMessage decode: target.
        self assert: rlc dst = 16r770801.
        self assert: rlc src = 16r05043E.
        self assert: rlc toMessage asByteArray = target.
    ]

    testUdt [
	| target udt called calling |
	target := #(9 0 3 7 11 4 67 7 0 254 4 67 92 0 254 3 1 2 3) asByteArray.
	called := SCCPAddress createWith: 254 pointCode: 7.
	calling := SCCPAddress createWith: 254 pointCode: 92.
	udt := SCCPUDT
		    initWith: called
		    calling: calling
		    data: #(1 2 3) asByteArray.
	self assert: udt toMessage asByteArray = target.
	udt := SCCPMessage decode: target.
	self assert: (udt isKindOf: SCCPUDT).
	self assert: udt calledAddr ssn = 254.
	self assert: udt calledAddr pointCode = 7.
	self assert: udt callingAddr ssn = 254.
	self assert: udt callingAddr pointCode = 92.
	self assert: udt toMessage asByteArray = target
    ]

    testUDTClass [
        | target udt |
        target := #(9 129 3 13 24 10 18 6 0 18 4 83 132 9 0 55 11 18 7 0
                    18 4 54 25 8 0 4 49 70 100 68 73 4 81 1 3 78 107 42
                    40 40 6 7 0 17 134 5 1 1 1 160 29 97 27 128 2 7 128
                    161 9 6 7 4 0 0 1 0 27 3 162 3 2 1 0 163 5 161 3 2 1
                    0 108 128 162 12 2 1 64 48 7 2 1 67 48 2 128 0 0 0) asByteArray.

        udt := SCCPMessage decode: target.
        self assert: udt udtClass = 1.
        self assert: udt errorHandling = 8.
        self assert: udt toMessage asByteArray = target.
    ]

    testIT [
        | target it |
        target := #(16 1 3 176 191 0 3 240 36 66 239) asByteArray.

        it := SCCPMessage decode: target.
        self assert: it src = 16r0300BF.
        self assert: it dst = 16rB00301.
        self assert: it credit = 16rEF.
        self assert: it seq = #(16r24 16r42) asByteArray.
        self assert: it protoClass = 16rF0.

        self assert: it toMessage asByteArray = target.
    ]

    testAddrFromByteArray [
        | byte |
        byte := #(191 0 3) asByteArray.
        self assert: (SCCPAddrReference fromByteArray: byte) = 16r0300BF
    ]

    testAddrGTIFromByteArrray [
        | addr parsed gti |
        addr := #(16r0A 16r12 16r06 16r0 16r12 16r04 16r53 16r84 16r09 16r00 16r37) asByteArray.
        parsed := SCCPAddress parseFrom: addr.

        self assert: parsed ssn = SCCPAddress ssnHLR.
        self assert: parsed asByteArray = addr.

        "Now test the GTI parsing"
        gti := parsed gtiAsParsed.
        self assert: gti translation = 0.
        self assert: gti plan = SCCPGlobalTitle npISDN.
        self assert: gti nature = SCCPGlobalTitle naiInternationalNumber.
        self assert: gti addr = '3548900073'.
        parsed gtiFromAddr: gti.
        self assert: parsed asByteArray = addr.

    ]

    testAddrGTIOdd [
        | addr parsed gti |
        addr := #(16r0B 16r12 16r08 16r00 16r11 16r04 16r64 16r07 16r97 16r36 16r71 16r03) asByteArray.
        parsed := SCCPAddress parseFrom: addr.

        self assert: parsed ssn = SCCPAddress ssnMSC.
        self assert: parsed asByteArray = addr.

        "GTI encoding.."
        gti := parsed gtiAsParsed.
        self assert: gti translation = 0.
        self assert: gti plan = SCCPGlobalTitle npISDN.
        self assert: gti nature = SCCPGlobalTitle naiInternationalNumber.
        self assert: gti addr = '46707963173'.
        parsed gtiFromAddr: gti.
        self assert: parsed asByteArray = addr.
    ]
]

TestCase subclass: MessageBufferTest [
    <category: 'OsmoNetwork-Tests'>

    testAdd [
        | msg1 msg2 msg3 msg_master |
        msg1 := MessageBuffer new.
        msg2 := MessageBuffer new.
        msg3 := MessageBuffer new.

        msg1 putByteArray: #(1 2 3) asByteArray.
        msg2 putByteArray: #(4 5 6) asByteArray.
        msg3 putByteArray: #(7 8 9) asByteArray.

        msg_master := MessageBuffer new.
        msg_master putByteArray: msg1.
        msg_master putByteArray: msg2.
        msg_master putByteArray: msg3.

        self assert: msg_master size = 9.
        self assert: msg_master toByteArray = #(1 2 3 4 5 6 7 8 9) asByteArray.
        self assert: msg_master asByteArray = #(1 2 3 4 5 6 7 8 9) asByteArray.
    ]

    testEmptyByteArray [
        | msg |
        msg := MessageBuffer new.
        msg putByteArray: ByteArray new.
        self assert: msg size = 0.
        self assert: msg toByteArray = #() asByteArray.
    ]

    testPrependByteArray [
        | msg |
        msg := MessageBuffer new.
        msg putByteArray: #(3 4 5) asByteArray.
        msg prependByteArray: #(1 2) asByteArray.
        self assert: msg toByteArray = #(1 2 3 4 5) asByteArray.

        msg := MessageBuffer new.
        msg prependByteArray: #(1 2) asByteArray.
        msg putByteArray: #(3 4 5) asByteArray.
        self assert: msg toByteArray = #(1 2 3 4 5) asByteArray.

        msg := MessageBuffer new.
        msg prependByteArray: #(1 2) asByteArray.
        self assert: msg toByteArray = #(1 2) asByteArray.
    ]

    testIdentity [
        | msg |
        msg := MessageBuffer new.
        self assert: msg toMessage == msg.
    ]
]

TestCase subclass: M2UAMSGTests [
    <category: 'OsmoNetwork-M2UA-Tests'>

    testUnique [
        "This should have some sanity checks on the enum"
    ]

    testParseTag [
        | inp tag |
        inp := #(16r00 16r11 16r00 16r08 16rAC 16r10 16r01 16r51) asByteArray.
        tag := M2UATag fromStream: inp readStream.

        self assert: tag nr = 16r11.
        self assert: tag data = #(16rAC 16r10 16r01 16r51) asByteArray.
    ]

    testCreateTag [
        | tag exp |
        tag := M2UATag initWith: 16r11 data: (ByteArray new: 3 withAll: 6).
        exp := #(16r00 16r11 16r00 16r07 16r06 16r06 16r06 16r00) asByteArray.
        self assert: tag toMessage asByteArray = exp.
    ]

    testCreateMessage [
        | msg data out res |
        res := #(16r01 16r00 16r03 16r01 16r00 16r00 16r00 16r10
                 16r00 16r11 16r00 16r08 16rAC 16r10 16r01 16r51) asByteArray.
        data := #(16rAC 16r10 16r01 16r51) asByteArray.

        msg := M2UAMSG fromClass: M2UAConstants clsASPSM type: M2UAConstants aspsmUp.
        msg addTag: (M2UATag initWith: 16r11 data: data).
        out := msg toMessage asByteArray.

        self assert: out = res.
    ]

    testCreatePaddingMessage [
        | msg data out res |
        res := #(16r01 16r00 16r03 16r01 16r00 16r00 16r00 16r10
                 16r00 16r11 16r00 16r07 16rAC 16r10 16r01 16r00) asByteArray.
        data := #(16rAC 16r10 16r01) asByteArray.

        msg := M2UAMSG fromClass: M2UAConstants clsASPSM type: M2UAConstants aspsmUp.
        msg addTag: (M2UATag initWith: 16r11 data: data).
        out := msg toMessage asByteArray.

        self assert: out = res.
    ]

    testParseMessage [
        | inp msg |

        inp := #(16r01 16r00 16r03 16r01 16r00 16r00 16r00 16r10
                 16r00 16r11 16r00 16r08 16rAC 16r10 16r01 16r51) asByteArray.
        msg := M2UAMSG parseFrom: inp.
        self assert: msg msgClass = UAConstants clsASPSM.
        self assert: msg msgType = UAConstants aspsmUp.

        inp := #(16r01 16r00 16r06 16r01 16r00 16r00 16r00 16r2C
                 16r00 16r01 16r00 16r08 16r00 16r00 16r00 16r00
                 16r03 16r00 16r00 16r1A 16r81 16r5C 16r00 16r07
                 16r00 16r11 16rF0 16rAA 16rAA 16rAA 16rAA 16rAA
                 16rAA 16rAA 16rAA 16rAA 16rAA 16rAA 16rAA 16rAA
                 16rAA 16rAA 16r00 16r00) asByteArray.
        msg := M2UAMSG parseFrom: inp.
        self assert: msg msgClass = UAConstants clsMAUP.
        self assert: msg msgType = UAConstants maupData.
    ]

    testFindTag [
        | inp msg tag |
        inp := #(16r01 16r00 16r03 16r01 16r00 16r00 16r00 16r10 16r00
                    16r11 16r00 16r08 16rAC 16r10 16r01 16r51) asByteArray.
        msg := M2UAMSG parseFrom: inp.
        tag := msg findTag: M2UAConstants tagReserved ifAbsent: [ nil ].
        self assert: tag isNil.

        tag := msg findTag: M2UAConstants tagAspIdent ifAbsent: [self fail].
        self deny: tag isNil.
    ]
]

TestCase subclass: OsmoUDPSocketTest [
    <category: 'OsmoNetwork-Tests'>

    createSocket [
        ^ Sockets.DatagramSocket new.
    ]

    testSocketCreation [
        | socket rx tx |
        socket := OsmoUDPSocket new
            name: 'Test Socket';
            start: self createSocket;
            yourself.

        "Verify that we are in processing"
        rx := socket instVarNamed: #rx.
        tx := socket instVarNamed: #tx.
        self deny: rx isTerminated.
        self deny: tx isTerminated.

        socket stop.
        self assert: rx isTerminated.
        self assert: tx isTerminated.
    ]
]

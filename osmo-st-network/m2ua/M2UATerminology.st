"
 (C) 2011-2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: M2UATerminology [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: 'I attempt to help with the terminology for M2UA.

M2UA is defined in IETF RFC 3331 and is actually from a family
of closely related RFCs for M3UA, SUA, M2PA.

The whole idea is that one can adapt the M2UA layer from the classlic
E1/T1 timeslots to the more modern SCTP (SIGTRAN). MTP3 and above will
not notice the difference.

The communication for M2UA is between two systems, both should be
configurable as either a client or server (listening for incoming SCTP
connections).

In general the communication is between a Signalling Gateway
(SG) and a Media Gateway Controller (MGC). In our world the MGC
would is the MSC/HLR/VLR/AuC.

What makes things complicated is the cardinality of systems. There is
an Application Server (AS), this can have multiple Application Server
Processes (ASP) for one or multiple MTP links. While the RFC onlys
says that the SG should the list of ASs in practice both ends need to
do it.'>
]

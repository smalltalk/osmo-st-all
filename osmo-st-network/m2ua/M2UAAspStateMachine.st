"
 (C) 2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: M2UAAspStateMachine [
    | state |
    
    <category: 'OsmoNetwork-M2UA-States'>
    <comment: 'I am the Application Server Process State machine. An
pplication Server Process will create me to manage the state. My state
machine is driven by calling the selectors from the events protocol.
If you ask for an illegal state transition a DNU will be raised. Ath
this point you should probably reset what you are doing and do proper
error reporting.

This class is currently not used!'>

    M2UAAspStateMachine class >> initialState [
	^M2UAAspStateDown
    ]

    M2UAAspStateMachine class >> new [
	^(self basicNew)
	    initialize;
	    yourself
    ]

    entered: aState [
	aState entered

	"TODO notify users of the machine"
    ]

    initialize [
	state := self class initialState on: self
    ]

    left: aState [
	aState left

	"TODO notify users of the machine"
    ]

    moveToState: aNewState [
	| oldState |
	oldState := state.
	state := (aNewState new)
		    machine: self;
		    yourself.
	self left: oldState.
	self entered: state
    ]

    state [
	^state class
    ]

    aspActive: anEvent [
	<category: 'events'>
	state onAspActive: anEvent
    ]

    aspDown: anEvent [
	<category: 'events'>
	state onAspDown: anEvent
    ]

    aspInactive: anEvent [
	<category: 'events'>
	state onAspInactive: anEvent
    ]

    aspUp: anEvent [
	<category: 'events'>
	state onAspUp: anEvent
    ]

    otherAspInAsOverrides: anEvent [
	<category: 'events'>
	state onOtherAspInAsOverrides: anEvent
    ]

    sctpCdi: anEvent [
	<category: 'events'>
	state onSctpCdi: anEvent
    ]

    sctpRi: anEvent [
	<category: 'events'>
	state onSctpRi: anEvent
    ]
]

"
 (C) 2010-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Collection subclass: MessageBuffer [
    | chunks |

    <category: 'OsmoNetwork-Message'>
    <comment: 'A network buffer/creation class. Modeled after the msgb of osmocore'>

    MessageBuffer class >> new [
        <category: 'creation'>
        ^ (super new)
            initialize;
            yourself
    ]

    initialize [
        <category: 'accessing'>
        chunks := OrderedCollection new.
    ]

    toMessage [
        <category: 'creation'>
        ^ self
    ]

    prependByteArray: aByteArray [
        <category: 'creation'>
        chunks addFirst: aByteArray.
    ]    

    putByte: aByte [
        <category: 'creation'>
        chunks add: (ByteArray with: aByte)
    ]

    putByteArray: aByteArray [
        <category: 'creation'>
        chunks add: aByteArray.
    ]

    put16: aInt [
        | data low high |
        <category: 'creation'>
        low := (aInt bitAnd: 16rFF).
        high := (aInt bitShift: -8) bitAnd: 16rFF.
        data := ByteArray with: low with: high.
        chunks add: data.
    ]

    putLen16: aInt [
        | data low high |
        <category: 'creation'>
        low := (aInt bitShift: -8) bitAnd: 16rFF.
        high := aInt bitAnd: 16rFF.
        data := ByteArray with: low with: high.
        chunks add: data.
    ]

    putLen32: aInt [
        | a b c d data |
        <category: 'creation'>
        a := (aInt bitShift: -24) bitAnd: 16rFF.
        b := (aInt bitShift: -16) bitAnd: 16rFF.
        c := (aInt bitShift:  -8) bitAnd: 16rFF.
        d := (aInt bitShift:   0) bitAnd: 16rFF.
        data := ByteArray with: a with: b with: c with: d.
        chunks add: data.
    ]

    toByteArray [
        <category: 'deprecated'>
        ^ self asByteArray.
    ]

    size [
        "Count of how much data we have collected"
        <category: 'accessing'>
	^ chunks inject: 0 into: [:acc :each | acc + each size ]
    ]

    do: aBlock [
        <category: 'accessing'>
        chunks do: [:chunk |
            chunk do: aBlock.
        ].
    ]
]

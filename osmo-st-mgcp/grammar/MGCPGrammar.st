"
 (C) 2010-2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: 'PetitParser'.

PP.PPCompositeParser subclass: MGCPGrammar [
    | MGCPMessage EOL One_WSP MGCPMessage MGCPCommandLine MGCPVerb transaction_id endpointName MGCPversion MGCPParameter MGCPCommand ParameterValue SDPRecord SDPLine SDPinformation MGCPResponseLine responseCode responseString packageName |
    <category: 'OsmoMGCP-Core'>
    <comment: 'I am a the Grammar of the Media Gateway Control Protocol'>

    start [
        <category: 'accessing'>
        ^ MGCPMessage end
    ]

    EOL [
        <category: 'grammar-common'>
        ^ (Character cr asParser, Character lf asParser) /
           Character lf asParser
    ]

    One_WSP [
        <category: 'grammar-common'>
        ^ #blank asParser plus
    ]

    MGCPMessage [
        <category: 'grammar-common'>
        ^ MGCPCommand / self MGCPResponse
    ]

    MGCPCommandLine [
        <category: 'grammar-cmd'>
        ^ self MGCPVerb,
          self One_WSP,
          self transaction_id,
          self One_WSP,
          self endpointName,
          self One_WSP,
          self MGCPversion,
          self EOL
    ]

    MGCPVerb [
        <category: 'grammar-cmd'>
        ^ 'EPCF' asParser /
          'CRCX' asParser /
          'MDCX' asParser /
          'DLCX' asParser /
          'RQNT' asParser /
          'NTFY' asParser /
          'AUEP' asParser /
          'AUCX' asParser /
          'RSIP' asParser
    ]

    transaction_id [
        <category: 'grammar-cmd'>
        "Add Osmocom extension that starts with 'nat-'"
        ^ ((#digit asParser) min: 1 max: 9) flatten /
          ('nat-' asParser, ((#digit asParser) min: 1 max: 9) plus) flatten
    ]

    endpointName [
        <category: 'grammar-cmd'>
        "simplified version"
        ^ #word asParser star flatten, $@ asParser, #word asParser star flatten
    ]

    MGCPversion [
        <category: 'grammar-cmd'>
        "skipping the optional profilename for now"
        ^ 'MGCP' asParser, One_WSP, #digit asParser, $. asParser, #digit asParser
    ]

    MGCPCommand [
        <category: 'grammar-cmd'>
        ^ MGCPCommandLine, MGCPParameter star, SDPRecord optional
    ]

    MGCPParameter [
        <category: 'grammar-cmd'>
        ^ ParameterValue, EOL
    ]

    wordParser [
        ^ #word asParser / #punctuation asParser / ' ' asParser
    ]

    ParameterValue [
        ^ ($K asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($B asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($C asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($I asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($N asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($X asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($L asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($M asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($R asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($S asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($D asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($O asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($P asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($E asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($Z asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($Z asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('Z2' asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('I2' asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($F asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($Q asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($T asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('RM' asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('RD' asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ($A asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('ES' asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('PL' asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('MD' asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('X-Osmo-CP' asParser, $: asParser, #blank asParser star, self wordParser star flatten) /
          ('X-Osmux' asParser, $: asParser, #blank asParser star, self wordParser star flatten)
    ]

    MGCPResponse [
    "
     MGCPResponse = MGCPResponseLine 0*(MGCPParameter) *2(EOL *SDPinformation)
     The *2 appears to be weird
    "
        ^ MGCPResponseLine,
          MGCPParameter star, 
          SDPRecord optional
    ]

    responseCode [
        <category: 'response'>
        ^ (#digit asParser, #digit asParser, #digit asParser) flatten
    ]

    packageName [
        "Not Complete yet"
        "packageName = 1*(ALPHA / DIGIT / HYPHEN) ; Hyphen neither first or last"
        <category: 'response'>
        ^ #letter asParser plus
    ]

    responseString [
        <category: 'response'>
        ^ #letter asParser plus flatten
    ]

    MGCPResponseLine [
        ^ responseCode,
          self One_WSP,
          transaction_id,
          (self One_WSP, '/' asParser, packageName) optional,
          (self One_WSP, responseString) optional,
          EOL
    ]

    SDPRecord [
        ^ EOL, SDPinformation
    ]

    SDPinformation [
        ^ (SDPLine, EOL) plus
    ]

    SDPLine [
        ^ self wordParser star flatten
    ]
]

Eval [
    MGCPGrammar initialize.
]

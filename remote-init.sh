#!/bin/sh


init()
{
	echo $1
	git remote add -f rem-$1 $2
}

init osmo-st-core git://git.osmocom.org/smalltalk/osmo-st-core
init osmo-st-logging git://git.osmocom.org/smalltalk/osmo-st-logging
init osmo-st-network git://git.osmocom.org/smalltalk/osmo-st-network
init osmo-st-mgcp git://git.osmocom.org/smalltalk/osmo-st-mgcp
init osmo-st-asn1 git://git.osmocom.org/smalltalk/osmo-st-asn1
init osmo-st-gsm git://git.osmocom.org/smalltalk/osmo-st-gsm
init osmo-st-msc git://git.osmocom.org/smalltalk/osmo-st-msc
init osmo-st-sip git://git.osmocom.org/smalltalk/osmo-st-sip
init osmo-st-testphone git://git.osmocom.org/smalltalk/osmo-st-testphone
init petitparser git://gitorious.org/gnu-smalltalk-ports/petitparser
init petitparser-tests git://gitorious.org/gnu-smalltalk-ports/petitparser-tests.git
init soapopera git://gitorious.org/gnu-smalltalk-ports/soapopera.git
init osmo-st-openbsc-test git://git.osmocom.org/smalltalk/osmo-st-openbsc-test
init iliad-stable git://github.com/NicolasPetton/iliad-stable.git
init grease git://github.com/NicolasPetton/Grease.git
